<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'FAQs';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

        <!-- FAQs -->
        <section class="l-container c-faqs">

          <div class="xlarge-8 xlarge-centered">
            <div class=" mb4">
              <p class="PelvivaHeader color(blue-green) bold">FAQs</p>
            </div>

            <div>
              <p class="PelvivaSubheader color(green) bold">About Pelviva<sup>®</sup></p>
              
              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">What does Pelviva feel like?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Most women describe feeling a pulsing sensation although we will all feel it differently. Some women will feel it more than others, sometimes the pulse may change in strength as you move and some women have told us they feel it more on one side than the other. </p>

                      <p class="PelvivaCopy color(mid-grey)">Some women new to using Pelviva have told us that the first few devices seemed quite strong but that they got used to the sensation after using a few devices.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>


              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Is it too big?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">At first you may think Pelviva looks big, but after lots of research and testing we have made Pelviva the best size to work really well with your Pelvic Floor muscles. Pelviva is made of soft ‘squishy’ foam so it is easy to insert - especially if you use the included OptiLube™ gel. It is also very light and is really comfortable to wear.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How do you use Pelviva?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">For instructions on how to use Pelviva, please download our instruction leaflet or visit the How to use section of the website.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How often should I use Pelviva?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We recommend that you use Pelviva every other day, averaging up to 15 per month. Pelviva is designed for single use, and you should never re-use Pelviva as it will not work.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How old do I need to be to use Pelviva?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Pelviva is not recommended for use of woman under the age of 18, if you are under 18 we suggest you contact your local Health Professional to discuss your Pelvic Floor health.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Is Pelviva environmentally friendly?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Pelviva is an environmentally friendly alternative to pads. Improving the fitness of your Pelvic Floor muscles will mean less leaks. Less leaks means less pads and less load on landfill sites.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How should I dispose of Pelviva safely?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">You should dispose of Pelviva by placing it back in the pouch that it came in or by wrapping it in a tissue then putting it in your bin. Pelviva cannot be recycled, so you should dispose of it with your other household rubbish. DO NOT flush Pelviva down the toilet. DO NOT incinerate.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I move about when using Pelviva?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Of course. Pelviva is designed to fit like a tampon so you can get on with your day while using it.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">You should, however, not use Pelviva if you are doing any activity that may bring Pelviva in contact with water such as taking a bath, showering or swimming because this may stop it working.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Will I need to use Pelviva long term?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Like, any muscle the Pelvic Floor muscles will weaken if not exercised. Once you have built your Pelvic Floor fitness you’ll need to continue with a regular maintenance programme of Pelvic Floor exercises every day to keep it strong. You may choose to continue using Pelviva at a rate of 6 per month to maintain your Pelvic Floor muscle fitness.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I find it difficult to insert Pelviva. What can I do?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We advise applying a pea-sized amount of water-based lubricant (Optilube™ is included in the Pelviva pack) to the tip of the foam prior to removing the tab and inserting it. DO NOT use a petroleum based jelly or other sexual lubricants.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">You may find that it is easier to insert Pelviva in certain positions. Think about how you would insert a tampon and find a similar, comfortable position before you insert Pelviva. We suggest this could be with one leg placed on a step, or lying down with your head supported and your knees bent.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>
  
              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How long should I keep Pelviva in?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">You should keep Pelviva in for the entire 30 minute treatment programme and remove Pelviva after 30 minutes when the pulses have stopped.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How do I remove Pelviva?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Pull the removal cord to remove Pelviva. If you find this difficult, try lying down on the bed, relax and take a few deep breaths – gently pull on the cord and think about relaxing around your vaginal opening. Using the Optilube™ gel before inserting Pelviva will also make it easier to remove.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I can only feel it on one side. Is it working?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">The pulse programme creates a link with your muscles and the energy flows in one direction – sometimes this means that you only feel the sensation on one side or can actually only feel the stimulation when the cord is on a particular side (left or right) of the pull tab. Next time you use Pelviva, note which side of the tab, the removal cord is situated before you insert it. Try it on both sides and you will likely find that you have a favourite side. </p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I cannot feel anything happening when I insert Pelviva. Is it still working?<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Check that you have pulled out the ‘start’ tab and then immediately insert Pelviva. Leaving a Pelviva for more than 1 minute after pulling the tab will trigger a safety mechanism that stops the Pelviva from working Try applying a ‘pea sized’ amount of Optilube™ gel to the top of Pelviva prior to insertion to make sure Pelviva sits correctly inside the vagina. DO NOT use petroleum based jelly or other sexual lubricants.</p>

                      <p class="PelvivaCopy color(mid-grey)">Some women may find that they only feel the pulses when the cord is to a particular side of the pull tab. This is because the pulses flow in one direction and sometimes a traumatic childbirth can cause damage to the nerves down there. Next time you use a Pelviva note which side of the pull tab the removal cord is situated before you insert. If you still can't feel anything, try the other side the next time. You may develop a favourite side to use.</p>

                      <p class="PelvivaCopy color(mid-grey)">If after trying the above, you do not feel Pelviva working at all we recommend contacting a healthcare professional – a specialist nurse or continence physiotherapist in your local area, for a full assessment.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>
              
              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I can't seem to remove Pelviva. What should I do?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">If you have been struggling to remove Pelviva it is possible that your Pelvic Floor muscles have gone into spasm. Please try not to worry, the Pelviva cannot travel any further inside and is unlikely to do any damage if left inside for a couple of hours after the pulses have finished.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">Lie down and try to relax and forget about it for a few minutes. This will help your Pelvic Floor muscles to relax. You may want to watch some TV or read to distract yourself. When you are ready to remove Pelviva, apply some lubricant to the area and pull on the removal cord.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>
              
              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">After using Pelviva I have started bleeding and I am not due for a period.<img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">You may have a problem inside your vagina or with the cervix? You should stop using Pelviva immediately and contact your medical practitioner. Any bleeding between periods should be investigated.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">After using Pelviva I have started bleeding and my periods have already stopped (I am post menopause)?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">You may have a problem inside your vagina or with the cervix?</p>
                        
                      <p class="PelvivaCopy color(mid-grey)">You should stop using Pelviva immediately and contact your medical practitioner. Any bleeding after finishing having periods should be investigated.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <p class="PelvivaSubheader color(green) bold mt6">Do’s & Dont’s</p>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Should I use lubricant?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes – the included Optilube™ gel will make it more comfortable to insert Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I use Pelviva with an Intra-uterine device (IUD) or coil?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes, the carefully selected stimulation programme within Pelviva will not affect your IUD (coil). However if you think you may be pregnant you should not use Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I use Pelviva on holiday?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes, we recommend that Pelviva is packed in your checked-in luggage.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">Remember you must not use Pelviva when it may come into contact with water so you should not go swimming or have a bath with the device in place.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I do my Pelvic Floor exercises whilst using Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes – you can join in with the pulse programme which makes your muscles work for 10 seconds followed by a 10 seconds rest phase. To begin with you may only be able to join in with a few contractions but you will feel able to do more as your muscles get stronger.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I use Pelviva instead of a tampon?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No – Pelviva should not be used for menstruation.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I go to the toilet while I am using Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No. You should not use the toilet while you are using Pelviva. We recommend you empty your bladder before you pull the start tab. If you really need to go to the toilet during use, you must remove Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <p class="PelvivaSubheader color(green) bold mt6">Safety Information</p>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Are there any conditions that mean I should not use Pelviva
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes these include: -</p>

                      <ul class="list list--green">
                        <li class="list-item PelvivaCopy color(mid-grey)">Toxic Shock Syndrome</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Pelvic malignancy (cancer)</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Urinary tract infection (UTI))</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Vaginal infection / Thrush</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">If you are under 18</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">If you have an implanted Pacemaker or Sacral nerve stimulator</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">If you have a severe prolapse that you can feel at the opening of the vagina</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">If you have had Pelvic surgery in the last 12 weeks</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">During pregnancy and for up to 12 weeks post delivery</li>
                      </ul>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Are there any conditions that mean I should take extra care when using Pelviva
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes these include: - </p>

                      <ul class="list list--green">
                        <li class="list-item PelvivaCopy color(mid-grey)">High or low blood pressure</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Epilepsy</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">If you use a Diaphragm for contraception remove before using Pelviva</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Take care when inserting and removing Pelviva if you have vaginal / labial or clitoral piercings</li>
                        <li class="list-item PelvivaCopy color(mid-grey)">Dry  / itchy vagina (atrophic vaginitis)</li>
                      </ul>

                      <p class="PelvivaCopy color(mid-grey) bold">Please note: Pelviva is Nickel free and made of hypoallergenic materials so you should not have an allergic reaction to Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I use Pelviva while having a period?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No, we do not recommend the use of Pelviva during a period.</p>

                      <p class="PelvivaCopy color(mid-grey)">There is a possibility of increasing the blood flow and making your period heavier and more uncomfortable. There is also an increased risk of Toxic Shock Syndrome if you inadvertently leave Pelviva in after treatment. We recommend using 15 Pelviva each month during the treatment phase so you can fit in using these around your period.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have been using Pelviva and have found out I am pregnant. Will it have harmed the baby?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No, this is highly unlikely.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">Whilst we do not recommend using the product during pregnancy we do not expect Pelviva to have caused any harm to your baby. You should now stop using Pelviva and inform your obstetrician that you have been using it whilst you're pregnant.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have just had a baby. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We recommend waiting 12 weeks before commencing Pelviva to let your body recover from the birth.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have a heart pacemaker. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No, you should not use Pelviva.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">The electrical signal emitted from Pelviva could cause your Pacemaker to malfunction.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have a heart pacemaker. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No, you should not use Pelviva.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">The electrical signal emitted from Pelviva could cause your SNS to malfunction.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have a nickel allergy. Is it safe to use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes, the product does not contain any metal in the electrodes and is designed to be hypo-allergenic.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I've just had pelvic surgery (e.g. vaginal or abdominal Hysterectomy). Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We recommend waiting 12 weeks before commencing Pelviva to let you body recover from the surgery.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I had Toxic Shock Symdrome (TSS) as few years ago. Should I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No, we do not recommend using Pelviva as it is a vaginal device and could increase your risk of developing TSS again.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I had a pelvic malignancy ( e.g. cervical cancer) about 10 years ago and have now completed all my treatment and am discharged from the consultant can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Yes, as long as you have completed all your treatment and are now discharged you can use Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have a very dry itchy vagina. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">You should check that you do not have a vaginal infection or Thrush – contact your General Practitioner. Wait until your treatment is completed and the itching has stopped before commencing Pelviva.</p>
                                    
                      <p class="PelvivaCopy color(mid-grey)">If you have finished having your periods (post menopause) this could be a sign of atrophic vaginitis (lack of oestrogen in the vaginal tissues) – contact your General Practitioner who may be able to prescribe an oestrogen replacement therapy that is applied in the vagina. You should wait 12 weeks after commencing this treatment before using Pelviva.</p>
                        

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I have epilepsy. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We strongly recommend consulting the physician who looks after your epilepsy for advice before using Pelviva. It may be helpful to then have supervision from a specialist nurse or physiotherapist.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I think I may have a vaginal prolapse. Can I use Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">If you have a ‘lump’ or ‘bulge’ that you can feel at the opening of your vagina you should not start using Pelviva – you should contact a healthcare professional for a full assessment.</p>
                      
                      <p class="PelvivaCopy color(mid-grey)">If you have a smaller ‘lump’ or ‘bulge’ that does not come outside of the vagina you can use Pelviva which may help to improve the function of your Pelvic Floor muscles; improving Pelvic Floor muscle function has been shown to improve the symptoms of mild to moderate vaginal prolapse.</p>

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <p class="PelvivaSubheader color(green) bold mt6">Customer Service</p>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How much do Pelviva cost?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No answer currently available.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Can I get a sample of Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No answer currently available.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">I live outside the UK, how do I order Pelviva?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We do not currently supply Pelviva to countries outside the UK and Ireland. We do however want to hear from you if you do live outside the UK, as we do have plans to expand our business to other countries as we know women all over the world could benefit from using Pelviva. Please contact us or call us on 0800 681608 to tell us where you live as this information may help us decide which countries we should go to next.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">Will Femeda share or sell my data?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">We will only use your details to send you information about your order or about Pelviva. We will not sell your data to anyone who would market other goods and services to you. For full details on our privacy policy, please read our terms and conditions.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How long will my Pelviva take to arrive?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">Standard deliveries arrive within 3-5 days of your order.  If you wish to use our next day delivery service, please select this delivery method at checkout.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">How to change / cancel my order?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">No answer currently available.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>


              <?php /* question start */ ?>
                <div class="c-faqs__question-block mb2">
                  <p class="c-faqs__question PelvivaCopy-l semibold color(white) mb0 pa3">What is your returns policy?
                  <img class="c-faqs__question-arrow" src="assets/img/FAQs/faq-arrow.svg"/></p>

                  <div class="c-faqs__answer bck-color(super-light-grey)">
                    <div class="question-block-inner pa3">

                      <p class="PelvivaCopy color(mid-grey)">As Pelviva is an intimate device we cannot accept returns, however if your Pelviva’s are damaged please contact us and we will be happy to help.</p>
                      

                    </div>
                  </div>
                </div>
              <?php /* question end */ ?>

            </div>
          </div>

          <div class="clearfix"></div>
        </section>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
