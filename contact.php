<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Contact';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

    <div class="w-100 c-contactMap">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9131.557542585322!2d-1.603672!3d55.097701!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x84f763d2af7c1f1e!2sFemeda+Ltd!5e0!3m2!1sen!2suk!4v1516187228545" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <div class="w-100 bck-color(green) c-contactFormWrapper">
      <div class="column small-12 large-8 xxlarge-6 large-centered">
        <p class="PelvivaSubheader color(white) bold center mb2">Contact</p>
        <p class="Pelvivacopy color(white) center c-contactForm__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
      </div>

      <div class="column small large-8 xxlarge-6 large-centered c-conctactFormContainer">
        <form class="c-contactForm" action="#" method="post">
          <input type="text" id="name" class="c-contactField" name="name" placeholder="Name">

          <input type="email" id="email" class="c-contactField" name="email" placeholder="Email">

          <textarea id="message" name="message" class="c-contactField" placeholder="Message"></textarea>

          <input class="btn btn--contact" type="submit" value="Send">
        </form>
      </div>
    </div>

    <div class="l-container c-contactDetails">
      <div class="column small-12 large-6 xxlarge-4 large-centered">

        <div class="column small-12 large-6">
          <p class="Pelvivacopy bold color(blue-green)">Address</p>
          <ul class="c-contactDetails__list">
            <li class="Pelvivacopy">Femeda Ltd</li>
            <li class="Pelvivacopy">Unit 9, Nelson Park</li>
            <li class="Pelvivacopy">Colbourne Avenue</li>
            <li class="Pelvivacopy">Cramlington</li>
            <li class="Pelvivacopy">NE23 1WD</li>
          </ul>
        </div>

        <div class="column small-12 large-6">
          <p class="Pelvivacopy bold color(blue-green)">Contact</p>
          <ul class="c-contactDetails__list">
            <li class="Pelvivacopy">Email: info@pelviva.com</li>
            <li class="Pelvivacopy">Phone: +44 (0) 330 043 9838</li>
          </ul>
        </div>

      </div>
    </div>


    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
