<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Register';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>


    <div class="w-100 bck-color(green) c-contactFormWrapper">
      <div class="column small-12 large-8 xxlarge-6 large-centered">
        <p class="PelvivaSubheader color(white) bold center mb2">Register</p>
        <p class="Pelvivacopy color(white) center c-contactForm__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
      </div>

      <div class="column small large-8 xxlarge-6 large-centered c-conctactFormContainer">
        <form class="c-contactForm large-10 large-centered center">
          <input class="c-contactField" type="text" placeholder="Enter First name">
          <input class="c-contactField" type="text" placeholder="Enter Last name">
          <input class="c-contactField" type="text" placeholder="Enter Email Address">
          <div class="checkbox clearfix mb4">
            <input type="checkbox" name="tickbox-confirm" id="tickbox-confirm">
            <label for="tickbox-confirm" class="color(white)">
              Tick box. I have read, understood & agree to your Terms & Conditions and Privacy Policy. I understand that by submitting my details and proceeding to REGISTER, I agree to the Terms &amp; Conditons and the use of my personal information as set out in the Privacy Policy.
            </label>
          </div>
          <input class="btn btn--contact" type="submit" name="submit" value="Register now">
        </form>
      </div>
    </div>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
