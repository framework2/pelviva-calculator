<section class="l-container c-PelvicFloorAgeCalculator bck-color(white) touch-fix">

	<div class="l-container c-PelvicFloorAgeCalculator__container col-padding bck-color(blue-green)">

		<div class="column xlarge-8 giant-7 xlarge-centered">
			<h2 class="PelvivaSubheader color(white) bold center mb2-s mb3-l mb4-xl">Find out your Pelvic Floor age</h2>

			<p class="PelvivaCopy color(white) center">Is it time you started to take action to improve your Pelvic Floor muscle health? Complete our simple on line quiz which calculates how old your Pelvic Floor is.</p>

			<div class="clearfix"></div>
		</div>

		<div class=" column xlarge-12 super-9 xlarge-centered col-padding c-PelvicFloorAgeCalculator__wrapper">
			<div id="calculator">
			</div>

		</div>

		<div class="clearfix"></div>
	</div>

</section>
