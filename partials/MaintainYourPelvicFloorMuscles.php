<?php /* How often do you need to use pelviva */ ?>

<section class="l-container c-MaintainYourPelvicFloorMuscles bck-color(border-grey) touch-fix">

	<div class="column small-12 xlarge-6 c-MaintainYourPelvicFloorMuscles__text-wrapper col-padding bck-color(white-green)">
		
		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Maintain your Pelvic Floor Muscles</h2>

		<p class="PelvivaCopy color(dark-grey) mb3">You will need to maintain your muscle fitness. Like all muscles, those in your Pelvic Floor need to be regularly exercised to keep them strong and toned. Once you have completed your Pelvic Floor muscle re-training, we recommend you doing your own Pelvic Floor muscle exercises once every day or alternatively to help maintain your Pelvic Floor muscle fitness you may choose to use 6 Pelviva each month.</p>
	</div>

	<div class="column small-12 xlarge-6 col-no-padding  c-MaintainYourPelvicFloorMuscles__main-image"></div>

	<div class="clearfix"></div>

</section>