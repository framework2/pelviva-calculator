<section class="l-container c-DiscoverYourPelvicFloorAge">

  	<div class="column small-12 xxlarge-7 col-no-padding c-DiscoverYourPelvicFloorAge__image c-DiscoverYourPelvicFloorAge__Child c-DiscoverYourPelvicFloorAge__Child--left"></div>

 	 <div class="column small-12 xxlarge-5 col-padding c-DiscoverYourPelvicFloorAge__Child c-DiscoverYourPelvicFloorAge__Child--right bck-color(soft-moonstone)">
	     <h2 class="PelvivaSubheader c-PelvivaTestimonial__SubHeader color(blue-green) bold mb2-s mb4-xl">Discover your Pelvic Floor age</h2>

	     <p class="PelvivaCopy-l color(mid-grey) mb2-s mb4-xl mb5-xxl">Whatever your age, a strong Pelvic Floor is the key to better bladder control. Like any muscle, the Pelvic Floor can be strengthened by exercise and can help to prevent bladder leaks.</p>

	     <a class="btn btn--main c-PelvivaTestimonial--btn" href="#">Take the test</a>
	</div>

  <div class="clearfix">

  </div>


</section>
