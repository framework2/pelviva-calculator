<section class="l-container c-YourNotAlone bck-color(border-blue-green)">

	<div class="column small-12 xlarge-5 c-YourNotAlone__text-wrapper col-padding">

		<p class="PelvivaCopy-l color(dark-grey)  c-YourNotAlone__text mb2-s mb4-l semibold">If you have experienced bladder leakage either as a result of a sudden urge to go or from exercising, laughing, coughing or sneezing.</p>

		<h2 class="PelvivaSubheader color(blue-green) mb2-s mb4-xl bold">You’re not alone.</h2>

		<p class="PelvivaCopy-xl color(blue-green) c-YourNotAlone__text mb4-s">Bladder leaks can effect anyone of any age.</p>


		<a class="btn btn--main c-PelvivaTestimonial--btn uppercase" href="#">Buy Now</a>


	</div>

	<div class="column small-12 xlarge-7 col-no-padding c-YourNotAlone__main-image"></div>

	<div class="clearfix"></div>

</section>
