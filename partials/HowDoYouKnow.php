<section class="l-container c-HowDoYouKnow touch-fix bck-color(border-grey)">

	<div class="column small-12 xlarge-6 col-no-padding c-HowDoYouKnow__main-image"></div>

	<div class="column small-12 xlarge-6 c-HowDoYouKnow__text-wrapper col-padding bck-color(light-green)">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">How do you know Pelviva<sup>®</sup> is working?</h2>
		
		<p class="color(dark-grey) PelvivaCopy mb3">When you are using Pelviva you will feel a pulsing sensation that everyone experiences differently.</p>

		<p class="color(dark-grey) PelvivaCopy mb3">Some Pelviva may feel stronger than others depending on their position in your vagina, the activity that you are undertaking, your own anatomy and even the time of the month.</p>

		<p class="color(dark-grey) PelvivaCopy mb3">You may notice an improvement in symptoms within the first few days of using Pelviva as you become more aware of your Pelvic Floor muscles, but you should continue with the programme until you are sure that the improvement is lasting. Your Pelvic Floor muscles did not weaken overnight – be patient - it takes time to regain control.</p>

		<p class="color(dark-grey) PelvivaCopy mb3">With experience, you will learn how to use Pelviva to suit your body and obtain your best Pelvic Floor workout achieving less leaks and greater bladder control.</p>

		
	</div>

	<div class="clearfix"></div>

</section>

