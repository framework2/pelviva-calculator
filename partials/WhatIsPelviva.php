<section class="l-container bck-color(light-green)">

  <div class="c-WhatIsPelviva col-padding">

  	<div class="large-12 c-WhatIsPelviva__intro mb4 mb5-xl">
      <div class="column small-12 xxlarge-6 c-WhatIsPelviva__intro__text mr0 mr6-l pb3 pb4-xl mb4 mb0-xl">
        <h2 class="PelvivaHeader color(blue-green) bold mb3">What is Pelviva<sup>®</sup>?</h2>
        <p class="PelvivaCopy-l color(blue-green)">Pelviva® is a Pelvic Floor muscle re-trainer that provides women with a clinically effective, safe and discrete treatment for bladder leakage.</p>
      </div>

      <div class="column small-12 xxlarge-6 c-WhatIsPelviva__intro__feature">
        <a class=" btn btn--main mb4 mb0-l c-WhatIsPelviva__intro__feature__btn" href="#">BUY NOW</a>
          <img class="c-WhatIsPelviva__intro__feature__image" src="/assets/img/home/pelvivaBox.png"/>
      </div>

      <div class="clearfix"></div>

  	</div>


 	 <div class="large-12 c-WhatIsPelviva__pointers">

     <div class="column small-12 giant-6 col-no-padding mr6-g">

       <div class="c-WhatIsPelviva__pointer mb4 mb0-g">
         <div class="c-WhatIsPelviva__pointer__icon">
           <?php //svgIcon('ReactivePulseTechnologyIcon', '0 0 189.65 189.65', 'ReactivePulseTechnologyIcon');?>
           <img src="./assets/img/DynamicCRC/reactivePulseTechnology.svg"/>
         </div>

         <div class="c-WhatIsPelviva__pointer__content">
           <p class="PelvivaCopy-s pr1-s pl1-s pl2-super color(mid-grey)">It’s unique <span class="bold color(green)">Reactive Pulse Technology®</span> stimulates your Pelvic Floor muscles to do perfect contractions every time, enabling you to retrain your Pelvic Floor muscles and regain control of your bladder.</p>
         </div>

      </div>

     </div>

     <div class="column small-12 giant-6 col-no-padding">

       <div class="c-WhatIsPelviva__pointer">

         <div class="c-WhatIsPelviva__pointer__icon">
          <?php svgIcon('perfectFitIcon', '0 0 189.65 189.65', 'perfectFitIcon');?>
         </div>

         <div class="c-WhatIsPelviva__pointer__content">
           <p class="PelvivaCopy-s pr1-s pl1-s pl2-super color(mid-grey)">The <span class="bold color(green)">Dynamic CRC</span> – body responsive foam® makes Pelviva® comfortable and easy to use at any time of day, allowing you to ﬁt using Pelviva® into a busy lifestyle.</p>
         </div>

      </div>

     </div>

      <div class="clearfix"></div>

	  </div>
  </div>

</section>
