<?php /* Reasons Hero */ ?>

<section class="l-container c-reasonsHero bck-color(border-mid-green) col-no-padding">

	<div class="c-reasonsHero__image col-padding">
		<p class="c-reasonsHero__image__caption PelvivaSubheader color(blue-green) bold center xlarge-5">Totally discreet and simple to use, Pelviva<sup>®</sup> fits easily into your everyday routine</p>
	</div>

</section>
