<section class="l-container c-WhatIsBladderLeakage bck-color(border-grey)">

	<div class="c-WhatIsBladderLeakage__content-wrapper">

		<div class="column show@xlarge large-6 xxlarge-4 c-WhatIsBladderLeakage__image-main col-no-padding"></div>


		<div class="column large-12 xlarge-6 xxlarge-8 c-WhatIsBladderLeakage__info-wrapper mb3 mb4-xl col-no-padding">

			<div class="column large-12 xxlarge-4 c-WhatIsBladderLeakage__pointer mt4 mt6-xl mb3 mb6-xl mr4-g col-no-padding">

				<a href="/page4.php" class="c-WhatIsBladderLeakage__pointer__icon">
				
					<img class="CalculateYourPelvicFloorAgeIcon" src="/assets/img/WhatIsBladderLeakage/CalculateYourPelvicFloorAge.svg"/>
				</a>

				<div class="c-WhatIsBladderLeakage__pointer__gradient-wrapper">
					<p class="c-WhatIsBladderLeakage__pointer__text PelvivaCopy color(green) bold center">Do you need to improve your Pelvic Floor muscle health?</p>


					<p class="c-WhatIsBladderLeakage__pointer__text PelvivaCopy color(mid-grey) bold center">Complete the quiz and find out your Pelvic Floor age.</p>
				</div>

			</div>

			<div class="column large-12 xxlarge-8 c-WhatIsBladderLeakage__text-wrapper col-padding">
				<h2 class="PelvivaSubheader color(blue-green) bold mb4">What is Bladder Leakage?</h2>

				<p class="PelvivaCopy color(dark-grey)  c-WhatIsBladderLeakage__text mb2-s mb4-xl">Bladder leakage means that you pass urine when you don’t mean too. It can range from a small dribble now and then, to large floods of urine. The medical term for bladder leakage is <span class="bold">‘urinary incontinence’</span> but most women don’t talk about the problem using the word ‘incontinence’ preferring to talk about bladder leakage.</p>

				<p class="PelvivaCopy color(dark-grey)  c-WhatIsBladderLeakage__text mb2-s mb4-xl">Leaks may be occasional or frequent depending on the severity and type of condition. The most likely cause of bladder leakage in women is Pelvic Floor muscle weakness.</p>

				<p class="PelvivaCopy-xl color(green) bold mb4">Are you in the habit of always popping to the toilet?</p>
				<p class="PelvivaCopy-xl color(green) bold mb4">So how many times a day should the average person urinate?</p>

				<a class="btn btn--main showInfoBox">Click to find out</a>
			</div>

		</div>

		<div class="clearfix"></div>

	</div>

</section>


<?php include 'partials/WhatIsBladderLeakage__popup.php'?>