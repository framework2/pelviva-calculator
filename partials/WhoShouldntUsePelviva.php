<section class="l-container c-HowDoYouKnow touch-fix bck-color(border-grey)">

	<div class="bck-color(white) col-padding">
		<h2 class="PelvivaSubheader color(blue-green) bold mb3 mb4-xl">Who shouldn’t use Pelviva<sup>®</sup></h2>

		<ul class="list mb4 mb6-xl">
			<li class="list-item color(dark-grey) semibold PelvivaCopy-l mb3">Please be aware Pelviva is not suitable for use in women who are pregnant, actively trying for a baby or have had a baby in the last 3 months</li>
			<li class="list-item color(dark-grey) semibold PelvivaCopy-l mb3">Do not use Pelviva if you have a pacemaker (cardiac or other type of pacemaker)</li>
			<li class="list-item color(dark-grey) semibold PelvivaCopy-l mb3">Do not use Pelviva if you are epileptic</li>
		</ul>
		<p class="PelvivaCopy-l color(dark-grey)">Before use, please read the full list of contraindications and warnings in the ‘Instruction for use leaflet’ (IFU) <a class="PelvivaCopyLink bold color(blue-green)">HERE.</a></p>
	</div>

</section>
