<?php /** Footer **/ ?>

<footer class="c-Footer bck-color(green)">
	<div class="c-Footer__top">
		<div class="l-container c-Footer__top-wrapper pt4 pt5-l pb4">
			<div class="column large-6 mb3 mb0-l">
				<a href="/" class="c-Footer__logo">
					<?php svgIcon('pelviva-logo-white', '0 0 189.65 54.84', 'pelviva-logo-white');?>
				</a>
			</div>

<!-- 			<div class="column large-6 c-Footer__login-wrapper">
				<a href="" class="c-Footer__login">
						<div class="c-Footer__login__icon__wrapper">
							<div class="c-Footer__login__icon">
								<?php //svgIcon('padlock', '0 0 25.76 28.07', 'padlock');?>
							</div>
						</div>
						<p class="PelvivaCopy color(white) bold c-Footer__login__text uppercase">HCP Login</p>
				</a>
			</div> -->

			<div class="clearfix"></div>
		</div>
	</div>

	<div class="c-Footer__bottom l-container pt5 pb6">

		<div class="c-Footer__bottom__info column large-6 mb4 mb0-l">
			<p class="PelvivaFooterCopy color(white) mb4">© Femeda 2017 All Rights Reserved.<br>
			 This site is brought to you by Femeda Ltd,<br>
			 Unit 9, Nelson Park, Colbourne Avenue,<br>
			 Cramlington, NE23 1WD</p>

			 <p class="PelvivaFooterCopy color(white)">item code / Date of prep</p>
		</div>

		<div class="c-Footer__bottom__links column large-6">
			<a href="https://www.facebook.com/" target="_blank" class="c-Footer__bottom__social-icon mr2">
				<?php svgIcon('facebook-round-new', '0 0 32 32', 'facebook-round-new');?>
			</a>
			<a href="https://www.twitter.com/" target="_blank" class="c-Footer__bottom__social-icon mr4">
				<?php svgIcon('twitter-round-new', '0 0 32 32', 'twitter-round-new');?>
			</a>

			<ul class="c-Footer__bottom__navigation mt2 mt0-l">
				<li class="PelvivaFooterCopy color(white)"><a class="c-Footer__bottom__navigation__link" href="terms.php">Terms</a></li>
				<li class="PelvivaFooterCopy color(white)"><a class="c-Footer__bottom__navigation__link" href="privacy.php">Privacy</a></li>
			</ul>
		</div>

		<div class="clearfix"></div>

	</div>


</footer>

<script src="assets/js/app.js"></script>
