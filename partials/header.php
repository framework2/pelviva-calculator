<div class="c-topBar w-100">

  <a class="basketIcon__wrapper">
    <div class="basketIcon"><?php svgIcon('basket', '0 0 29.27 24.4', 'basket');?></div>
    <p class="basketIcon__text">BUY NOW</p>
  </a>

</div>

<div class="w-100 Header__Wrapper">

  <div class="l-container Header__InnerWrapper">
    <div class="column small-6 Header__Child Header__Child--left">
      <a href="home.php" class="pelvivaLogo">
        <?php svgIcon('pelviva-logo-blue', '0 0 432.45 125.05', 'pelviva-logo-blue');?>
      </a>
    </div>

    <div class="column small-6 Header__Child Header__Child--right">
      <div class="burgerMenu color(blue-green) menu-toggle">
        <span class="burgerMenu__text">Menu</span>
        <span class="burgerMenu__icon">&#9776;</span>
      </div>
    </div>

  </div>

</div>

  <div class="c-topBar__border w-100"></div>
