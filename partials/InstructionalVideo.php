<?php /* Reasons Hero */ ?>

<section class="l-container c-InstructionalVideo bck-color(border-grey) touch-fix">

	<div class="column small-12 xlarge-6 col-padding bck-color(white)">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Re-train your Pelvic Floor muscles. Regain your bladder control</h2>

		<h3 class="PelvivaCopy-xl color(dark-grey) mb3 mb5-xl">Simply follow our step-by-step instructional video</h3>

		<p class="PelvivaCopy">Before use, please read the full list of contraindications and warnings in the ‘Instruction for use leaflet’ (IFU) <a href="/resources/downloads/pelviva.pdf" target="_blank" class="bold color(blue-green) PelvivaCopyLink">here.</a></p>
	</div>

 	<div class="column small-12 xlarge-6 col-padding bck-color(white)">
 		<div class="c-InstructionalVideo__video">
 			<video width="100%" height="100%" poster="/assets/img/video/video-placeholder.jpg"> 
 			 
 			 <!-- video format depending on what the browser supports -->
 			 <source src="/assets/video/pelviva_video.mp4" type="video/mp4" />
 			 <source src="/assets/video/pelviva_video.webm" type="video/webm" />
 			 <source src="/assets/video/pelviva_video.ogg" type="video/ogg" />
 			 Sorry, your browser does not support this video player

 			</video> 
 		</div>
 	</div>

	<div class="clearfix"></div>

</section>
