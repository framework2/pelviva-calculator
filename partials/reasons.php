<section class="l-container c-reasons bck-color(border-grey)">

	<div class="bck-color(white) w-100 c-reasons__intro col-padding">
		<div class="bck-color(bermuda) pa3 pa4-l pa5-xl">
			<h2 class="PelvivaSubheader bold center color(blue-green) mb3">6 Reasons to use Pelviva<sup>®</sup> - an effective and easy to use
			treatment for female bladder leakage</h2>

			<p class="PelvivaCopy-xl color(white) bold center">If you aren’t sure of the benefits of using Pelviva already, this is a must read for you</p>
		</div>
	</div>


	<div class="column xlarge-6 c-reasons__text-wrapper col-padding bck-color(white) touch-fix-2">

		<div id="textbox__hover1" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Convenient & discreet</h2>

			<ul class="list list--green mb3">
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Pelviva fits into women's busy lifestyles</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Easy to use at any time of the day</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Only takes 30 minutes</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l">Recommended usage every other day for 12 weeks</li>
			</ul>

		</div>

		<div id="textbox__hover2" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Unique Reactive Pulse Technology</h2>

			<ul class="list list--green mb3">
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Each Pelviva contains a microprocessor that delivers an innovative treatment programme to re-train or rehabilitate weak Pelvic Floor muscles</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Pelviva uses a unique Reactive Pulse Technology which, by using a non-uniform stimulation programme delivers both low and moderate frequencies, these frequencies target both power (fast twitch) and endurance (slow twitch) muscle fibre in the same treatment. The power muscle fibres are needed to control bladder leaks with coughing and laughing or exercising (stress leakage) and the endurance muscle fibres are required to prevent leakage when urgently needing to pass urine (urgency leakage). </li>
			</ul>

		</div>

		<div id="textbox__hover3" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Innovative Microprocessor Technology</h2>

			<ul class="list list--green mb3">
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The Reactive Pulse Technology, ensures an effective therapeutic level of muscle stimulation is delivered to the Pelvic Floor muscles throughout each 30 minute treatment.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Pelviva is fully automated and also includes a feedback mechanism to ensure an effective therapeutic level of muscle stimulation is delivered to the Pelvic Floor muscles.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The automated intensity removes the risk present in conventional muscle stimulation that the user assumes they have reached an effective treatment level.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The feedback checks every pulse returning from the muscle throughout the 30 minute programme and automatically adjusts upwards or downwards to maintain the effective target intensity. This allows Pelviva to adapt to changes in body position during use allowing you to remain active whilst using Pelviva.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The feedback mechanism in the Pelviva also adapts to changes in muscle physiology and can adapt to differing female anatomies.</li>
			</ul>

		</div>

		<div id="textbox__hover4" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Up to 25% Deeper Penetration</h2>

			<ul class="list list--green mb3">

				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The innovative non-uniform frequency incorporates a 'doublet' pulse at the beginning of each pulse train. This allows the surface layer of muscle to be activated by the first pulse and allows the second pulse to be received by a further layer of muscle, unlocking up to 25% more of the muscle to receive the subsequent pulses than is seen with conventional muscle stimulation.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">This non-uniform pattern of stimulation has also been shown to improve cortical awareness of muscle activity which is very important when evidence shows that up to 50% of women don’t know how to do their Pelvic Floor exercises correctly. </li>

			</ul>

		</div>

		<div id="textbox__hover5" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Dynamic CRC Body Responsive Foam</h2>

			<ul class="list list--green mb3">
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Made from soft, compressible foam Pelviva adapts to your individual shape and size to ensure a good fit holding the pulse pads in close proximity to the Pelvic Floor muscles and allowing you to move comfortably during treatment.</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">The soft foam electrode allows the Pelvic Floor muscles to shorten and contract without meeting the resistance seen with conventional, rigid hard plastic electrodes.</li>
			</ul>
		</div>

		<div id="textbox__hover6" class="c-reasons__textbox">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold"> Clinically Proven</h2>

			<ul class="list list--green mb3">

				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">In a randomised clinical study, 84% of women using Pelviva for 12 weeks reported an improvement in bladder leakage</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Women using Pelviva for 12 weeks also experienced a four times greater reduction in the impact that bladder leaks had on their lives</li>
				<li class="list-item semibold color(dark-grey) PelvivaCopy-l mb3">Pelviva treats the symptoms of stress, urgency and mixed bladder leakage</li>

			</ul>

		</div>

	</div>

	<div class="column xlarge-6 col-padding bck-color(white) touch-fix-2">

		<p class="bold PelvivaCopy-l color(blue-green) mb4 mb6-xl">Scroll over icons to highlight Pelviva® unique features</p>


		<div class="c-reasons__icon-container">

			<div id="hover1" class="c-reasons__icon-wrapper mb4">
				<div class="c-reasons__icon">
				  <?php svgIcon('perfectFitIcon', '0 0 189.65 189.65', 'perfectFitIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover2" class="c-reasons__icon-wrapper mb4">
				<div class="c-reasons__icon">
				  <?php svgIcon('ReactivePulseTechnologyIcon', '0 0 189.65 189.65', 'ReactivePulseTechnologyIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover3" class="c-reasons__icon-wrapper mb4">
				<div class="c-reasons__icon">
				  <?php svgIcon('MicroProcessorTechnologyIcon', '0 0 189.65 189.65', 'MicroProcessorTechnologyIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover4" class="c-reasons__icon-wrapper">
				<div class="c-reasons__icon">
				  <?php svgIcon('DeeperPenetrationIcon', '0 0 189.65 189.65', 'DeeperPenetrationIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover5" class="c-reasons__icon-wrapper">
				<div class="c-reasons__icon">
				  <?php svgIcon('dynamicCRCIcon', '0 0 189.65 189.65', 'reasonsIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover6" class="c-reasons__icon-wrapper">
				<div class="c-reasons__icon">
				  <?php svgIcon('30MinuteTreatmentIcon', '0 0 189.65 189.65', '30MinuteTreatmentIcon');?>
				</div>

				<div class="c-reasons__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

		</div>

	</div>


	<div class="clearfix"></div>

</section>

<div class="l-container bck-color(border-grey) touch-fix center">
	<a href="" class="btn btn--main ">BUY NOW</a>
</div>
