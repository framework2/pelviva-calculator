<?php /* How often do you need to use pelviva */ ?>

<section class="l-container c-HowOften bck-color(border-grey) touch-fix">

	<div class="column small-12 xlarge-6 col-no-padding c-HowOften__main-image"></div>

	<div class="column small-12 xlarge-6 c-HowOften__text-wrapper col-padding">
		
		<h2 class="PelvivaSubheader color(super-light-green-blue) mb4 bold">How often should you use Pelviva<sup>®</sup>?</h2>

		<p class="PelvivaCopy color(dark-grey) mb3">To retrain your Pelvic Floor muscles we recommend that you use Pelviva for <span class="color(blue-green) semibold">12 WEEKS</span>. You should use <span class="color(blue-green) semibold">15 Pelviva®</span> per month (approximately using one every other day). If you forget to use Pelviva® or choose not to use it due to menstruation, use one every day to ensure that you use 15 in a month.</p>

		<p class="PelvivaCopy color(dark-grey) mb3 mb6-super">You may notice an improvement in symptoms within the first few days of using Pelviva as you make the connection between your brain and Pelvic Floor muscle, but you should continue <img class="hide show@super c-HowOften__text-image" src="/assets/img/Reasons/reasonsHero2.png"/>the programme until you are sure that the improvement is lasting. It takes up to <span class="color(blue-green) semibold">12 WEEKS</span> to see the strengthening effect of Pelvic Floor exercises.</p>

		<img class="show@small hide@super c-HowOften__text-image mb3" src="/assets/img/Reasons/reasonsHero2.png"/>

		<a class="btn btn--main c-PelvivaTestimonial--btn uppercase" href="#">Buy Now</a>
	</div>

	<div class="clearfix"></div>

</section>