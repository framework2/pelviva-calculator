<?php
$login_successful = false;

if( isset( $_SERVER['PHP_AUTH_USER'] ) && isset( $_SERVER['PHP_AUTH_PW'] ) ) {

    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];

    if ($username === 'demo' && $password === 'pelviva123'){
        $login_successful = true;
    }

}

if ( !$login_successful ){

    header('WWW-Authenticate: Basic realm="Framework Staging"');
    header('HTTP/1.0 401 Unauthorized');

    die( '<center><h1>401 Authorization Required</h1></center>' );

}
