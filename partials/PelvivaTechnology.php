<div class="l-container c-PelvivaTechnology">
	<div class="column xlarge-6 c-PelvivaTechnology__video-wrapper bck-color(bullet-grey) col-padding">
		<h2 class="PelvivaSubheader color(blue-green) mb4 semibold">How <span class="bold">Pelviva<sup>®</sup></span> works</h2>

		<div class="c-PelvivaTechnology__video">
			
			<video width="100%" height="100%" poster="/assets/img/video/video-placeholder.jpg"> 
			 
			 <!-- video format depending on what the browser supports -->
			 <source src="/assets/video/pelviva_video.mp4" type="video/mp4" />
			 <source src="/assets/video/pelviva_video.webm" type="video/webm" />
			 <source src="/assets/video/pelviva_video.ogg" type="video/ogg" />
			 Sorry, your browser does not support this video player

			</video> 
		</div>
	</div>

	<div class="column xlarge-6 bck-color(bullet-grey) col-padding">
		<h2 class="PelvivaSubheader color(blue-green) mb4 semibold"><span class="bold">Pelviva<sup>®</sup></span> Technology</h2>

		<div class="c-PelvivaTechnology__interactive-element col-padding">
			<p class="color(blue-green) bold PelvivaCopy mb3 mb5-l hide show@xlarge">Hover over hot spot buttons to reveal Pelviva labels</p>

			<img class="show@small hide@xlarge c-PelvivaTechnology__main-image mb3" src="/assets/img/PelvivaTechnology/PelvivaTechnology-main-image--mobile.png"/>

			<div class="c-PelvivaTechnology__interactive-element__image-wrapper">

				<img class="hide show@xlarge c-PelvivaTechnology__main-image mb3" src="/assets/img/PelvivaTechnology/PelvivaTechnology-main-image.png"/>

				<div id="option1" class="c-PelvivaTechnology__interactive-element__hover-icon c-PelvivaTechnology__interactive-element__hover-icon--1 hide show@xlarge">
				  <?php svgIcon('PelvivaTechnologyHoverIcon', '0 0 59.08 59.1', 'PelvivaTechnologyHoverIcon');?>
				</div>

				<div id="option2" class="c-PelvivaTechnology__interactive-element__hover-icon c-PelvivaTechnology__interactive-element__hover-icon--2 hide show@xlarge">
				  <?php svgIcon('PelvivaTechnologyHoverIcon', '0 0 59.08 59.1', 'PelvivaTechnologyHoverIcon');?>
				</div>

				<div id="option3" class="c-PelvivaTechnology__interactive-element__hover-icon c-PelvivaTechnology__interactive-element__hover-icon--3 hide show@xlarge">
				  <?php svgIcon('PelvivaTechnologyHoverIcon', '0 0 59.08 59.1', 'PelvivaTechnologyHoverIcon');?>
				</div>

				<div id="option4" class="c-PelvivaTechnology__interactive-element__hover-icon c-PelvivaTechnology__interactive-element__hover-icon--4 hide show@xlarge">
				  <?php svgIcon('PelvivaTechnologyHoverIcon', '0 0 59.08 59.1', 'PelvivaTechnologyHoverIcon');?>
				</div>

				<div id="option5" class="c-PelvivaTechnology__interactive-element__hover-icon c-PelvivaTechnology__interactive-element__hover-icon--5 hide show@xlarge">
				  <?php svgIcon('PelvivaTechnologyHoverIcon', '0 0 59.08 59.1', 'PelvivaTechnologyHoverIcon');?>
				</div>

			</div>

			<div id="box__option1" class="c-PelvivaTechnology__interactive-element__hover-text mb3">
				<p class="uppercase bold color(mid-grey) PelvivaCopy-l"><span class="hide@xlarge">1. </span>Pull Tab</p>

				<p class="color(blue-green) PelvivaCopy-s">Allows a 10 second delay before treatment programme starts giving you plenty of time to insert Pelviva inside your vagina.</p>
			</div>

			<div id="box__option2" class="c-PelvivaTechnology__interactive-element__hover-text mb3">
				<p class="uppercase bold color(mid-grey) PelvivaCopy-l"><span class="hide@xlarge">2. </span>Dynamic CRC Body Responsive Foam</p>

				<p class="color(blue-green) PelvivaCopy-s">Soft, compressible foam makes Pelviva® easy to position in the vagina. Once in position, the foam reforms and adapts to fit every woman’s individual shape.</p>
			</div>

			<div id="box__option3" class="c-PelvivaTechnology__interactive-element__hover-text mb3">
				<p class="uppercase bold color(mid-grey) PelvivaCopy-l"><span class="hide@xlarge">2. </span>Flexible pulse pads</p>

				<p class="color(blue-green) PelvivaCopy-s">After giving you 10 seconds to position Pelviva® in your vagina, the strength of the pulsed stimulation gradually increases to a level that effectively exercises your Pelvic Floor muscles for you.</p>
			</div>

			<div id="box__option4" class="c-PelvivaTechnology__interactive-element__hover-text mb3">
				<p class="uppercase bold color(mid-grey) PelvivaCopy-l"><span class="hide@xlarge">2. </span>Microprocessor</p>

				<p class="color(blue-green) PelvivaCopy-s">The microprocessor delivers the unique Pelviva® Reactive Pulse Technology.</p>
			</div>

			<div id="box__option5" class="c-PelvivaTechnology__interactive-element__hover-text">
				<p class="uppercase bold color(mid-grey) PelvivaCopy-l"><span class="hide@xlarge">2. </span>Battery</p>

				<p class="color(blue-green) PelvivaCopy-s">Optimised to deliver a 30 minute treatment programme.</p>
			</div>



		</div>
	</div>

	<div class="clearfix"></div>
</div>
