<section class="l-container bck-color(white)">

	<div class="c-WhyExerciseYourPelvicFloor col-no-padding">

		<div class="column small-12 xlarge-5 col-no-padding c-WhyExerciseYourPelvicFloor__main-image"></div>

		<div class="column small-12 xlarge-7 c-WhyExerciseYourPelvicFloor__text-wrapper col-padding">

			<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Why exercise your Pelvic Floor?</h2>

			<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">Women focus quite a bit of attention on improving muscle tone in the arms, legs, bottom and tummy, yet many neglect an equally important but invisible area – the Pelvic Floor muscles.</p>

			<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">The good news is that like all muscles, the Pelvic Floor muscle can be strengthened through regular exercise – giving you back bladder control.</p>

			<h2 class="PelvivaCopy-xl color(blue-green) mb4 bold">Signs and symptoms of weak Pelvic Floor muscles</h2>
			
			<ul class="list list--green mb4">
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Leaks with cough, sneeze or laugh</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Leaks when you exercise</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Leaks when you start to run</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Leaks on the trampoline</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Bowel control problems</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Heavy, dragging feeling in the vagina</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold mb3">Lower back pain</li>
				<li class="list-item PelvivaCopy-l color(dark-grey) semibold ">Sexual problems</li>
			</ul>

			<p class="PelvivaCopy-xl color(blue-green) c-WhyExerciseYourPelvicFloor__text mb4 bold">Common causes of weakened Pelvic Floor muscles</p>

			<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">Life events like pregnancy, childbirth, menopause, getting older and surgery can all weaken the Pelvic Floor muscles that support the bladder: The most common causes are: </p>
		
		</div>

		<div class="column small-12 c-WhyExerciseYourPelvicFloor__btn-wrapper col-padding">

			<a class="btn btn--outline SmoothScroll mb3 mb0-xxl" href="#pregnancy">Pregnancy & Childbirth</a>

			<a class="btn btn--outline SmoothScroll mb3 mb0-xxl" href="#chronic-straining">Chronic Straining</a>

			<a class="btn btn--outline SmoothScroll mb3 mb0-xxl" href="#menopause">Menopause</a>

			<a class="btn btn--outline SmoothScroll" href="#surgery">Surgery</a>		

		</div>

		<div class="clearfix"></div>

	</div>

</section>


<?php 
	include 'partials/WhyExerciseYourPelvicFloor-sections/WhyExerciseYourPelvicFloor-pregnancy.php';
	include 'partials/WhyExerciseYourPelvicFloor-sections/WhyExerciseYourPelvicFloor-menopause.php';
	include 'partials/WhyExerciseYourPelvicFloor-sections/WhyExerciseYourPelvicFloor-straining.php';
	include 'partials/WhyExerciseYourPelvicFloor-sections/WhyExerciseYourPelvicFloor-surgery.php';
 ?>

