<section class="l-container c-WhatIsThePelvicFloor bck-color(border-grey) touch-fix">

	<div class="c-WhatIsThePelvicFloor__content-wrapper">

		<div class="column small-12 xxlarge-6 c-WhatIsBladderLeakage__text-wrapper col-padding">
			<h2 class="PelvivaSubheader color(green) mb4 bold">What is the pelvic floor?</h2>

			<p class="PelvivaCopy color(dark-grey)  c-WhatIsThePelvicFloor__text mb2-s mb4-xl">All women (and men) have a Pelvic Floor muscle. The Pelvic Floor is a set of muscles that sit like a hammock between your tail bone (coccyx) and the bone between your legs at the front of the pelvis (pubic bone). Pelvic Floor muscles give us control over our bladder and bowel. They also support the pelvic organs and span the bottom of the pelvis.</p>

			<p class="PelvivaCopy color(dark-grey)  c-WhatIsThePelvicFloor__text">Weakened Pelvic Floor muscles mean the internal organs are not fully supported which may lead to symptoms of a prolapse and you may have diffculty controlling your bladder.</p>
		</div>


		<div class="column small-12 xxlarge-6 c-WhatIsThePelvicFloor__image-main-wrapper">
				<img class="c-WhatIsThePelvicFloor__image-main" src="/assets/img/WhatIsThePelvicFloor/pelvic-floor-diagram.svg"/>
		</div>

		<div class="clearfix"></div>

	</div>

</section>
