<section class="l-container c-DynamicCRC touch-fix">

	<div class="column xlarge-6 col-padding">


		<p class="PelvivaCopy-l color(white) bck-color(light-green-blue) semibold pa3 mb5 hide show@xlarge">The Pelviva team have brought together a range of innovative technology to make Pelviva clinically effective and easy to use.</p>

		<div class="c-DynamicCRC__text-wrapper">

			<div id="textbox__hover1" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">Body Responsive Foam</h2>

				<p class="PelvivaCopy color(mid-grey)">The foam is soft, lightweight, and can be easily compressed to position inside the vagina. It is designed to maximise comfort and product performance. No matter what shape or size you are, once in position the Pelviva foam reforms and moulds to comfortably fit your individual shape; holding the flexible pulse pads in contact with your body for effective treatment.</p>

			</div>

			<div id="textbox__hover2" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">Reactive Pulse Technology</h2>
				<p class="PelvivaCopy color(mid-grey)">RPT monitors every pulse delivered to your Pelvic Floor muscles and allows Pelviva to adapt to your body:</p>
				<ul>
					<li class="PelvivaCopy color(mid-grey)">To changes in body position during use, allowing you to remain active whilst using Pelviva</li>
					<li class="PelvivaCopy color(mid-grey)">To changes in muscle physiology and differing female anatomies.</li>
				</ul>

			</div>

			<div id="textbox__hover3" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">Soft & Comfortable Fit</h2>

				<p class="PelvivaCopy color(mid-grey)">The soft compressible bio-compatible foam makes Pelviva easy to insert and comfortable to wear. The flexible pulse pads allow your body to move whilst having your Pelviva® treatment.</p>

			</div>

			<div id="textbox__hover4" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">Microprocessor Technology</h2>

				<p class="PelvivaCopy color(mid-grey)">Each Pelviva contains a microprocessor that monitors the treatment level and delivers an innovative electrical stimulation to re-train and strengthen the Pelvic Floor muscles.</p>

			</div>

			<div id="textbox__hover5" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">30 Minute Treatment</h2>

				<p class="PelvivaCopy color(mid-grey)">Each Pelviva is programmed to run a 30 minute re-training treatment for your Pelvic Floor muscles. These pulses are designed to target both types of your Pelvic Floor muscles. When the treatment has finished Pelviva is easily removed by pulling gently on the cord.</p>

			</div>

			<div id="textbox__hover6" class="c-DynamicCRC__textbox">

				<h2 class="PelvivaSubheader color(blue-green) mb3 bold">25% Deeper Penetration</h2>

				<p class="PelvivaCopy color(mid-grey)">Pelviva is programmed to deliver a unique pattern of pulses to your Pelvic Floor muscles, which means it can penetrate up to 25% deeper into your Pelvic Floor muscles than stimulation devices that do not contain this pattern. This deeper penetration also helps you make the neuromuscular connection between the brain and the Pelvic Floor muscles, making it easier to do your own Pelvic Floor exercises without the Pelviva device.</p>

			</div>
		</div>

	</div>

	<div class="column xlarge-6 col-padding">

		<p class="bold PelvivaCopy-l color(blue-green) mb4 mb6-xl">Scroll over icons to highlight Pelviva<sup>®</sup> unique features</p>

		<div class="c-DynamicCRC__icon-container">

			<div id="hover1" class="c-DynamicCRC__icon-wrapper mb4">
				<div class="c-DynamicCRC__icon">
				  <?php svgIcon('dynamicCRCIcon', '0 0 189.65 189.65', 'dynamicCRCIcon');?>
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover2" class="c-DynamicCRC__icon-wrapper mb4">
				<div class="c-DynamicCRC__icon">
				  <?php// svgIcon('ReactivePulseTechnologyIcon', '0 0 189.65 189.65', 'ReactivePulseTechnologyIcon');?>
				  <img src="./assets/img/DynamicCRC/reactivePulseTechnology.svg"/>
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover3" class="c-DynamicCRC__icon-wrapper mb4">
				<div class="c-DynamicCRC__icon">
				  <?php //svgIcon('perfectFitIcon', '0 0 189.65 189.65', 'perfectFitIcon');?>
					<img src="/assets/img/home/Perfect-Fit-Icon.png" alt="soft comfortable fit">
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover4" class="c-DynamicCRC__icon-wrapper">
				<div class="c-DynamicCRC__icon">
				  <?php svgIcon('MicroProcessorTechnologyIcon', '0 0 189.65 189.65', 'MicroProcessorTechnologyIcon');?>
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover5" class="c-DynamicCRC__icon-wrapper">
				<div class="c-DynamicCRC__icon">
				  <?php svgIcon('30MinuteTreatmentIcon', '0 0 189.65 189.65', '30MinuteTreatmentIcon');?>
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>

			<div id="hover6" class="c-DynamicCRC__icon-wrapper ">
				<div class="c-DynamicCRC__icon">
				  <?php //svgIcon('DeeperPenetrationIcon', '0 0 189.65 189.65', 'DeeperPenetrationIcon');?>
					<img src="/assets/img/home/25-percent-Deeper-Icon.png" alt="25 Percent Deeper Icon ">
				</div>

				<div class="c-DynamicCRC__icon-shadow">
				  <?php svgIcon('IconShadow', '0 0 151 24.04', 'IconShadow');?>
				</div>
			</div>
		</div>

	</div>


	<div class="clearfix"></div>

</section>
