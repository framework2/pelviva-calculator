<section class="l-container c-WomenHero bck-color(white-green)">

	<div class="c-WomenHero__content-wrapper l-container col-no-padding">

	  	<div class="column small-12 xlarge-5 col-padding c-WomenHero__Child c-WomenHero__Child--left">

	      <h2 class="PelvivaHeader color(blue-green) c-WomenHero__Header bold">1 in every 3 women</h2>
	      <p class="PelvivaSubheader color(blue-green) c-WomenHero__SubHeader semibold">experiences bladder leaks at some time in their lives</p>

	      <p class="PelvivaCopy color(dark-grey) c-WomenHero__copy">The NHS recommends Pelvic Floor Muscle Exercises as its first line treatment- but research shows that up to 50% of women  have problems doing their Pelvic Floor Exercises correctly – and if you do them incorrectly you can make matters worse.</p>

	  	</div>

	 	 <div class="column small-12 xlarge-7 c-WomenHero__Child c-WomenHero__Child--right"></div>

	 	 <div class="clearfix"></div>
	</div>


</section>
