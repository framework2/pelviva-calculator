<section class="l-container c-WhyPelviva bck-color(light-green)">
    
    <div class="column large-12 col-padding bck-color(green) c-WhyPelviva__header">
      <h1 class="PelvivaSubheader color(white) semibold">Why <span class="bold">Peliviva<sup>®</sup>?</span></h1>
    </div>


    <div class="column small-12 xlarge-6 col-padding  c-WhyPelviva__Child bck-color(green)">

      <p class="PelvivaCopy color(blue-green)">
        Pelviva is a single use, disposable medical device made from soft compressible foam. Each Pelviva contains a microprocessor that delivers the revolutionary <span class="bold">Pelviva Reactive Pulse Technology</span> directly to your Pelvic Floor muslces, exercising both types of muscle fibre within your Pelvic Floor. The Pelviva reactive pulse mimics the way your body works naturally, stimulating your pelvic floor muscles to perform perfect Pelvic Floor contractions every time. Helping you recognise the feeling of correct Pelvic Floor muscle exercises.
      </p>

      <div class="c-WhyPelviva__ImageWrapper">
      <img class="c-WhyPelviva__Image" src="./assets/img/whyPelviva/whyPelvivaMain.svg" alt=""/>
      </div>

    </div>

    <div class="column small-12 xlarge-6 col-padding c-WhyPelviva__Child bck-color(green)">

      <p class="PelvivaCopy b color(blue-green) c-WhyPelviva__Copy--top">The two types of muscle ﬁbre within your Pelvic Floor:</p>

      <div class="c-WhyPelviva__Fibre1 mb3-s">
        <p class="PelvivaCopy-l color(blue-green) mb0">POWER FIBRES</p>
        <p class="PelvivaCopy color(white)">Need to have speed and strength so they can stop leakage when you cough, sneeze, laugh or exercise.</p>
      </div>

      <div class="c-WhyPelviva__Fibre2 mb2-s mb4-l">
        <p class="PelvivaCopy-l color(blue-green) mb0">ENDURANCE FIBRES</p>
        <p class="PelvivaCopy color(white)">Need to work to help you hold on when your urgently need the toilet and to stop you needing the toilet too frequently. Because of the two fiber types, different exercises are rquired to improve their individual performance.</p>
      </div>

      <p class="PelvivaCopy color(blue-green) c-WhyPelviva__Copy--bottom"><span class="bold">Pelviva<sup>®</sup></span> is the only treatment to provide one combined treatment for both stress and urinary incontinence.</p>

    </div>






</section>

