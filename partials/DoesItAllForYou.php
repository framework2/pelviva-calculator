<?php /* Does it all for you */ ?>

<section class="l-container c-DoesItAllForYou bck-color(border-grey) touch-fix">

	<div class="column small-12 xxlarge-6 bck-color(light-green-blue) c-DoesItAllForYou__info">

		<div class="c-DoesItAllForYou__info__text-wrapper">
			<h2 class="PelvivaSubheader color(white) mb6-s mb8-xxl bold">Peliviva® – Does it all for you</h2>

			<h2 class="PelvivaCopy-l color(blue-green) center mb6-s mb8-xxl">TEXT TO WRITE AND DROP IN HERE</h2>
		</div>

		<img class="c-DoesItAllForYou__image-main" src="/assets/img/DoesItAllForYou/DoesItAllForYou-main-image.png"/>

	</div>

	<div class="column small-12 xxlarge-6 col-padding bck-color(light-green-blue) c-DoesItAllForYou__points">

		<div class="c-DoesItAllForYou__points__block mb3">
			<p class="PelvivaCopy-l color(blue-green) c-DoesItAllForYou__points__text">The Pelviva® programme runs for <span class="color(white) uppercase">30 MINUTES</span> with a series of 10 second pulses and rest periods.</p>

			<div class="c-DoesItAllForYou__points__icon mb3 mb0-xl">
				<?php svgIcon('30MinuteTreatmentIcon', '0 0 189.65 189.65', '30MinuteTreatmentIcon');?>
			</div>

		</div>

		<div class="c-DoesItAllForYou__points__block mb3 mb0-xl">
			<p class="PelvivaCopy-l color(blue-green) c-DoesItAllForYou__points__text">Pelviva®’s <span class="color(white) uppercase">REACTIVE PULSE</span> automatically adjust to your body altering stimulation level based on your muscle strength and tone.</p>
			<div class="c-DoesItAllForYou__points__icon mb3 mb0-xl">
				<?php svgIcon('ReactivePulseTechnologyIcon', '0 0 189.65 189.65', 'ReactivePulseTechnologyIcon');?>
			</div>
		</div>

		<div class="c-DoesItAllForYou__points__block mb3 mb0-xl">
			<p class="PelvivaCopy-l color(blue-green) c-DoesItAllForYou__points__text">Innovative <span class="color(white) uppercase">MICROPROCESOR</span> delivers an electrical stimulation treatment</p>

			<div class="c-DoesItAllForYou__points__icon mb3 mb0-xl">
				<?php svgIcon('MicroProcessorTechnologyIcon', '0 0 189.65 189.65', 'MicroProcessorTechnologyIcon');?>
			</div>
		</div>

		<div class="c-DoesItAllForYou__points__block">
			<p class="PelvivaCopy-l color(blue-green) c-DoesItAllForYou__points__text">Contains a <span class="color(white) uppercase">DOUBLE PULSE</span> that delivers <span class="color(white) uppercase">25%</span> deeper stimulation into the Pelvic Floor muscles.</p>

			<div class="c-DoesItAllForYou__points__icon mb3 mb0-xl">
				<?php svgIcon('DeeperPenetrationIcon', '0 0 189.65 189.65', 'DeeperPenetrationIcon');?>
			</div>
		</div>

	</div>


	<div class="clearfix"></div>

</section>
