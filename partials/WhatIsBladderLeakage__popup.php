<div class="l-container c-WhatIsBladderLeakage__answer c-WhatIsBladderLeakage__answer--hidden bck-color(border-grey)">
	<div class="bck-color(light-green-blue)">
		<p class="color(white) PelvivaSubheader bold center column large-9 large-centered col-padding mb0">“The normal frequency for going to the toilet is between 4 – 6 times a day”</p>

		<div class="clearfix"></div>
	</div>
</div>