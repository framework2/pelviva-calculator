<section class="l-container c-WhyExerciseYourPelvicFloor c-WhyExerciseYourPelvicFloor--inner bck-color(border-grey) touch-fix" id="menopause">

	<div class="column small-12 xlarge-5 col-no-padding c-WhyExerciseYourPelvicFloor__main-image c-WhyExerciseYourPelvicFloor__main-image--menopause"></div>

	<div class="column small-12 xlarge-7 c-WhyExerciseYourPelvicFloor__text-wrapper c-WhyExerciseYourPelvicFloor__text-wrapper--menopause col-padding">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Menopause</h2>

		<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">The bladder, vagina and bladder outlet tube (urethra) are all sensitive to the hormone oestrogen. As you go through the menopause your ovaries will gradually produce less oestrogen which can cause symptoms such as dryness, itching and irritation in the vagina, bladder tube and bladder. These changes can cause you to need to go to the toilet more frequently including having to get up at night to empty your bladder (nocturia). The menopause also accelerates the natural aging process which can cause mild symptoms of bladder leakage to quickly become more of a problem.</p>

		<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">Some women experience more bladder leakage with the hormone changes in the menstrual cycle with symptoms seeming to be worse just before a period or mid-cycle during ovulation.
		</p>
	
	</div>

	<div class="clearfix"></div>

</section>

