<section class="l-container c-WhyExerciseYourPelvicFloor c-WhyExerciseYourPelvicFloor--inner bck-color(border-grey) touch-fix" id="chronic-straining">

	<div class="column small-12 xlarge-5 col-no-padding c-WhyExerciseYourPelvicFloor__main-image c-WhyExerciseYourPelvicFloor__main-image--straining"></div>

	<div class="column small-12 xlarge-7 c-WhyExerciseYourPelvicFloor__text-wrapper c-WhyExerciseYourPelvicFloor__text-wrapper--straining col-padding">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Chronic Straining</h2>

		<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">A chronic cough can add to the strain on the Pelvic Floor muscles. It’s good to talk to your GP to see if you can have treatment for a chronic cough.</p>

		<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">Having trouble emptying your bowels and needing to strain to help them to work can cause a lot of additional stress to the Pelvic Floor muscles. Your GP can help you manage symptoms of constipation or may refer you to a specialist nurse or physiotherapist who can advise on treatment.
		</p>
	
	</div>

	<div class="clearfix"></div>

</section>

