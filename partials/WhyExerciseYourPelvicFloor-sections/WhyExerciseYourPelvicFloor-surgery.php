<section class="l-container c-WhyExerciseYourPelvicFloor c-WhyExerciseYourPelvicFloor--inner bck-color(border-grey) touch-fix" id="surgery">

	<div class="column small-12 xlarge-5 col-no-padding c-WhyExerciseYourPelvicFloor__main-image c-WhyExerciseYourPelvicFloor__main-image--surgery"></div>

	<div class="column small-12 xlarge-7 c-WhyExerciseYourPelvicFloor__text-wrapper c-WhyExerciseYourPelvicFloor__text-wrapper--surgery col-padding">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Surgery</h2>

		<p class="PelvivaCopy-l color(dark-grey)  c-WhyExerciseYourPelvicFloor__text mb4 semibold">Some women experience an increase in bladder leakage following abdominal surgery, for example, after a Hysterectomy or Gall bladder surgery (Cholecystectomy). It’s important to exercise your Pelvic Floor muscles after having any abdominal surgery, you can use Pelviva to help after 12 weeks.</p>
	
	</div>

	<div class="clearfix"></div>

</section>

