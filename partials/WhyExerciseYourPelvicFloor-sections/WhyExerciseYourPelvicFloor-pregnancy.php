<section class="l-container c-WhyExerciseYourPelvicFloor c-WhyExerciseYourPelvicFloor--inner bck-color(border-grey)" id="pregnancy">

	<div class="column small-12 xlarge-5 col-no-padding c-WhyExerciseYourPelvicFloor__main-image c-WhyExerciseYourPelvicFloor__main-image--pregnancy"></div>

	<div class="column small-12 xlarge-7 c-WhyExerciseYourPelvicFloor__text-wrapper c-WhyExerciseYourPelvicFloor__text-wrapper--pregnancy col-padding">

		<h2 class="PelvivaSubheader color(blue-green) mb4 bold">Pregnancy & Childbirth</h2>

		<p class="PelvivaCopy-l color(dark-grey) mb4 semibold">During pregnancy, hormonal changes and the increasing weight of Mum and the baby can place a lot of stress on your Pelvic Floor muscles, having an effect as early as 12 weeks.<p>

		<p class="PelvivaCopy-l color(dark-grey) mb4 semibold">Childbirth can have a further weakening effect on the Pelvic Floor especially if:<p>

		<ul class="list mb4">
			<li class="list-item PelvivaCopy-l color(dark-grey) mb3 semibold">it is a large baby (>4kg / 8lbs 12oz),</li>
			<li class="list-item PelvivaCopy-l color(dark-grey) mb3 semibold">you push for a long time or</li>
			<li class="list-item PelvivaCopy-l color(dark-grey) mb3 semibold">you have an instrumental delivery with Forceps or</li>			
			<li class="list-item PelvivaCopy-l color(dark-grey) semibold">Ventouse (vacuum assisted).</li>
		</ul>

		<p class="PelvivaCopy-l color(dark-grey) mb4 semibold">Women who have a caesarean section delivery may still experience problems with their Pelvic Floor muscles due to the changes during pregnancy so its important for all women to look after their Pelvic Floor muscles after delivery.<p>

		<p class="PelvivaCopy-l color(dark-grey) mb4 semibold">You can start to use Pelviva 12 weeks after your delivery.<p>

	
	</div>

	<div class="clearfix"></div>

</section>

