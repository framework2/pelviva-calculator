<section class="l-container c-howPelvivaWorks bck-color(white)">

  	<div class="column small-12 xxlarge-6 col-padding c-howPelvivaWorks__Child--left bck-color(soft-moonstone)">

      <h1 class="PelvivaSubheader c-howPelvivaWorks__Subheader color(blue-green) mb3-s mb5-l bold">How Pelviva® works</h1>

      <div class="c-howPelvivaWorks__video">

           <video width="100%" height="100%" poster="/assets/img/video/video-placeholder.jpg"> 
            
            <!-- video format depending on what the browser supports -->
            <source src="/assets/video/pelviva_video.mp4" type="video/mp4" />
            <source src="/assets/video/pelviva_video.webm" type="video/webm" />
            <source src="/assets/video/pelviva_video.ogg" type="video/ogg" />
            Sorry, your browser does not support this video player

           </video> 
      </div>

  	</div>

 	 <div class="column small-12 xxlarge-6 col-padding c-howPelvivaWorks__content bck-color(soft-moonstone)">

     <p class="PelvivaCopy-l c-howPelvivaWorks__copy color(mid-grey)"><span class="PelvivaCopySpan bold color(blue-green)">Pelviva re-trains both types of muscles in the Pelvic Floor</span> - power muscles to help prevent leaks when you cough, sneeze or exercise and endurance muscles to help you hold on when you urgently need the toilet.</p>

      <p class="PelvivaCopy-l c-howPelvivaWorks__copy color(mid-grey) mb4 mb5-super">Designed and developed to be easy to use while <img class="hide show@super c-howPelvivaWorks__image mt4" src="/assets/img/home/pelvivaBox.png"/>delivering a clinically proven treatment to improve bladder control.</p>

      <div class="c-howPelvivaWorks__bottom">
        <a class="btn btn--main c-howPelvivaWorks__btn" href="#">BUY NOW</a>
        <img class="show@small hide@super c-howPelvivaWorks__image" src="/assets/img/home/pelvivaBox.png">
      </div>

	  </div>


</section>
