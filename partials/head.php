<head>
  <meta charset="utf-8">
  <title>Pelviva - <?php echo $title; ?> </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" href="https://use.typekit.net/kxq8qoe.css">
  <link rel="stylesheet" href="/assets/css/all.css?unique=<?php echo date('d-m-y-h-i-s') ?>">
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicons/favicon-16x16.png">
  <link rel="manifest" href="/assets/favicons/manifest.json">
  <link rel="mask-icon" href="/assets/favicons/safari-pinned-tab.svg" color="#D0043C">
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
  <meta name="theme-color" content="#D0043C">
</head>
