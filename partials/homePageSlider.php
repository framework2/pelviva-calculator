
<div class="mainCarousel l-container">
  <section class="l-container c-homePageSlider__wrapper col-no-padding carousel-cell">
    <div class="c-homePageSlider">

      <div class="columns small-12 large-8 xxlarge-6 c-homePageSlider__content col-padding">
        <p class="PelvivaHeader color(blue-green) bold mb4-super">Pelviva<sup>®</sup></p>
        <p class="PelvivaCopy-xl color(blue-green) bold mb4-super">Life-changing technological<br class="hide show@xlarge"> breakthrough for weak Pelvic <br class="hide show@xlarge">Floor muscles that cause <br class="hide show@xlarge">bladder leaks</p>
        <a class="btn btn--main" href="#">Learn More</a>
      </div>


    </div>
  </section>




  <section class="l-container carousel-cell col-no-padding">
    <div class="c-homePageSlider2">
        <div class="columns small-12 xxlarge-6 col-padding">
          <p class="PelvivaHeader-super color(green) lineHeight1 mb0 bold">84%</p>
          <p class="PelvivaCopy-xl mt2-l color(blue-green) bold lineHeight1 mb0">of women using Pelviva®</p>
          <p class="PelvivaCopy-xl color(green) bold mb4-super lineHeight1">reported an improvement <br class="hide show@xxlarge">in bladder control</p>
            <a class="btn btn--main" href="#">Buy Now</a>
        </div>

        <div class="columns small-12 xxlarge-6 col-no-padding">
          <img class="hide show@xxlarge c-homePageSlider__image" src="/assets/img/home/pelvivaBoxSoft.png" alt=""></img>
        </div>



    </div>
  </section>
</div>
