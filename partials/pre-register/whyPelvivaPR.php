<section class="l-container c-WhyPelviva
c-WhyPelviva--pr bck-color(white)">
  	<div class="column small-12 xlarge-6 pt6-s pt0-xl pa0 bck-color(aqua-squeeze)">
      <div class="c-WhyPelviva__ImageWrapper">
        <img class="c-WhyPelviva__Image" src="./assets/img/whyPelviva/whyPelvivaMain.svg" alt=""/>
      </div>

  	</div>

    <div class="column small-12 xlarge-6 col-padding bck-color(aqua-squeeze)">

      <h2 class="PelvivaSubheader color(blue-green) semibold mb4">Why <span class="bold">Pelviva<sup>®</sup>?</span></h2>

      <p class="PelvivaCopy-l color(mid-grey) mb4">Up to half of women aren’t able to correctly exercise their Pelvic Floor muscles. Pelviva performs your Pelvic Floor exercises for you, so there’s no need to worry about whether you are doing them properly.</p>

      <p class="PelvivaCopy-l color(mid-grey) mb2-s mb3-l">Pelviva helps strengthen both types of Pelvic Floor muscles:</p>
      <ul class="list list--mark-green list--mark-green--l mb4-s mb6-l">
        <li class="list-item PelvivaCopy-l semibold color(blue-green) mb3">Power muscles that prevent leaks when you cough, sneeze or exercise</li>
        <li class="list-item PelvivaCopy-l semibold color(blue-green) mb3">Endurance muscles to help hold on when you urgently need the toilet</li>
      </ul>
    </div>

    <div class="clearfix"></div>

    <div class="small-12  bck-color(aqua-squeeze) col-padding touch-fix-2">
      <h3 class="PelvivaCopy-l color(white) center bck-color(surfie-green) regular pa4">Whatever causes your bladder leakage, Pelviva can help you by strengthening both your power and endurance Pelvic Floor muscles.</h3>
    </div>
</section>
