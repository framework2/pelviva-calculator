<section class="l-container c-preRegisterForm bck-color(swans-down)">

	<div class="column bck-color(sinbad) col-padding">
		<?php if(1==0): ?>
		<p class="PelvivaCopy-l bold color(white) center mb0 lineHeight1 large-10 large-centered mb3 mb5-xl">Pelviva is clinically proven to treat the symptoms of stress, urgency and mixed incontinence, discreetly, anywhere, at home, at the office, in the gym or walking the dog. To find out more pre-register here.</p>
		<?php endif; ?>
		<p class="PelvivaCopy-l color(white) semibold center mb0 lineHeight1 large-10 large-centered mb3">You don’t have to live with bladder leaks. Regain control of your Pelvic Floor muscles with Pelviva. To find out more, <a href="register.php" class="PelvivaCopy-l bold">sign up now.</a></p>
		<div class="large-centered center mb2-s mb4-l mb2-s mt4-l">
			<a href="register.php" class="btn btn--contact bck-color(border-blue-green) lh-title">Keep me updated</a>
		</div>
		<?php if(1==0): ?>
		<form class="c-preRegisterForm__form large-10 large-centered center">
			<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Name">
			<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Email Address">
			<input class="c-preRegisterForm__form__submit btn btn--contact bck-color(border-blue-green)" type="submit" name="submit" value="Pre-Register">
		</form>
		<?php endif; ?>

		<h3 class="PelvivaCopy-l bold color(white) center lineHeight1 c-preRegisterForm--icons-container">
			Follow us
			<div class="c-preRegisterForm--icons-wrapper mb2-s mt2-s mt0-large mb0-large">
				<a href="https://www.facebook.com/" target="_blank" class="c-preRegisterForm--icon mr2 ml3 color(white)">
					<?php svgIcon('facebook-round-new', ' 0 0 32 32', 'facebook-round-new');?>
				</a>
				<a href="https://www.twitter.com/" target="_blank" class="c-preRegisterForm--icon mr3 color(white)">
					<?php svgIcon('twitter-round-new', ' 0 0 32 32', 'twitter-round-new');?>
				</a>
			</div>
			for the latest news, expert opinions and patient stories
		</h3>
	</div>

	<div class="clearfix"></div>

</section>