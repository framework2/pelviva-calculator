<section class="l-container c-oneInThreeWomen bck-color(white) touch-fix">

  	<div class="column small-12 xxlarge-7 col-no-padding c-oneInThreeWomen__image c-oneInThreeWomen__Child c-oneInThreeWomen__Child--left"></div>

 	 <div class="column small-12 xxlarge-5 col-padding c-oneInThreeWomen__Child c-oneInThreeWomen__Child--right bck-color(soft-moonstone)">
	     <h2 class="PelvivaSubheader c-PelvivaTestimonial__SubHeader color(blue-green) bold mb2-s mb4-xl">1 in 3 women experience bladder leaks</h2>

	     <p class="PelvivaCopy-l color(mid-grey) mb3">If you have experienced bladder leakage, either after a sudden urge to go, or from exercising, laughing, coughing or sneezing,</p>

	     <h2 class="PelvivaCopy-xl color(blue-green) bold mb3">YOU'RE NOT ALONE</h2>

	     <p class="PelvivaCopy-l color(mid-grey)">Bladder leaks can affect anyone of any age.</p>
	</div>

  <div class="clearfix"></div>
</section>
