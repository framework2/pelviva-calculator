<section class="l-container c-preRegisterForm c-preRegisterForm--social bck-color(lightest-grey)">

	<div class="column col-no-padding">

		<div class="mb3 mb5-xl">
			<p class="PelvivaSubheader bold color(surfie-green) center lineHeight1 mb4-s mb5-xl">Pelviva is launching very soon</p>
			<p class="PelvivaCopy-l semibold color(puerto-rico) center small-centered mb4-s mb5-xl large-9">Sign up now and be the first to know when Pelviva is available</p>

			<div class="large-centered center mb4-s mb5-xl">
				<a href="register.php" class="btn btn--contact bck-color(border-blue-green) lh-title">Keep me updated</a>
			</div>

			<h3 class="PelvivaCopy-l bold color(surfie-green) center lineHeight1 c-preRegisterForm--icons-container">
				Follow us
				<div class="c-preRegisterForm--icons-wrapper mb2-s mt2-s mt0-large mb0-large">
					<a href="https://www.facebook.com/" target="_blank" class="c-preRegisterForm--icon mr2 ml3 color(border-blue-green)">
						<?php svgIcon('facebook-round-new', ' 0 0 32 32', 'facebook-round-new');?>
					</a>
					<a href="https://www.twitter.com/" target="_blank" class="c-preRegisterForm--icon mr3 color(border-blue-green)">
						<?php svgIcon('twitter-round-new', ' 0 0 32 32', 'twitter-round-new');?>
					</a>
				</div>
				for the latest news, expert opinions and patient stories
			</h3>
		</div>
		<?php if(1==0): ?>
		<form class="c-preRegisterForm__form large-10 large-centered center">
			<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Name">
			<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Email Address">
			<input class="c-preRegisterForm__form__submit btn btn--contact bck-color(border-blue-green)" type="submit" name="submit" value="Register now">
		</form>
		<?php endif; ?>
	</div>

	<div class="clearfix"></div>

</section>