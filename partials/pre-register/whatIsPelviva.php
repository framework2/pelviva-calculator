<section class="l-container c-whatIsPelviva-PR bck-color(white)">

   <div class="column small-12 xxlarge-6 col-padding c-whatIsPelviva-PR__Child--right bck-color(aqua-squeeze)">

    <h1 class="PelvivaSubheader c-whatIsPelviva-PR__Subheader color(blue-green) mb3 mb4-xl bold">How Pelviva<sup>®</sup> works</h1>

    <div class="c-whatIsPelviva-PR__video">
      
      <video width="100%" height="100%" poster="/assets/img/video/video-placeholder.jpg"> 
       
       <!-- video format depending on what the browser supports -->
       <source src="/assets/video/pelviva_video.mp4" type="video/mp4" />
       <source src="/assets/video/pelviva_video.webm" type="video/webm" />
       <source src="/assets/video/pelviva_video.ogg" type="video/ogg" />
       Sorry, your browser does not support this video player

      </video> 
      
    </div>


  </div>

  <div class="column small-12 xxlarge-6 col-padding c-whatIsPelviva-PR__Child--left bck-color(aqua-squeeze)">

    <h1 class="PelvivaSubheader c-whatIsPelviva-PR__Subheader color(blue-green) mb3-s bold">What is Pelviva<sup>®</sup>?</h1>

    <p class="PelvivaCopy-l c-whatIsPelviva-PR__copy color(mid-grey) mb4">An innovative, easy-to-use Pelvic Floor muscle re-trainer, providing clinically safe and discreet treatment for bladder leakage.</p>

    <p class="PelvivaCopy-l c-whatIsPelviva-PR__copy color(mid-grey) mb4">The disposable, single use device is made from soft compressible foam and contains flexible pulse pads that deliver a unique stimulation programme to your Pelvic Floor muscles.</p>

    <p class="PelvivaCopy-l c-whatIsPelviva-PR__copy color(mid-grey) mb4">Treatment is recommended every other day for 12 weeks.</p>     

    <!-- <ul class="list list--mark-green">
      <li class="list-item PelvivaCopy-l color(mid-grey) mb3">Clinically proven results when used for a 12 week treatment programme</li>
      <li class="list-item PelvivaCopy-l color(mid-grey)">Recommended usage every other day for 12 weeks</li>
    </ul> -->

  </div>


</section>
