<section class="l-container">

	<div class="c-introducingPelviva bck-color(ebb)">

		<div class="columns small-12 xxlarge-6 col-padding">
		  <h1 class="PelvivaHeader color(blue-green) bold mb3">Introducing Pelviva<sup>®</sup></h1>
		  <p class="PelvivaCopy-xl color(dark-grey) mb5">A clinically effective, discreet and easy-to-use Pelvic Floor muscle re-trainer for the treatment of bladder leakage in women.</p>
		</div>

		<div class="columns small-12 xxlarge-6 col-no-padding c-introducingPelviva__main-image-wrapper">
			<img class="c-introducingPelviva__main-image" src="/assets/img/home/pelvivaBoxSoft-smaller-height.png"/>
		</div>
		<div class="clearfix"></div>
		<div class="small-12 col-padding touch-fix-2 mt2" id="c-introducingPelviva__msgBar"><p class="PelvivaCopy-xl bold color(white) bck-color(border-blue-green) center pa3">Great news for all women: Pelviva is launching in 2018</p></div>

		

		<div class="col-padding touch-fix-2 small-12">
		<?php if(1==0): ?>
			<p class="color(dove-gray) semibold center PelvivaCopy-xl m-center mb4-s mb4-xl large-10">Pelviva treats the underlying cause of bladder leaks rather than just the symptoms. Want to know more? Get the latest information from Pelviva and Pre-register today.</p>
			<p class="PelvivaCopy-l bold color(surfie-green) center lineHeight1 mb4-s mb5-xl c-preRegisterForm--icons-container">
				Follow us on
				<a href="https://www.facebook.com/" target="_blank" class="c-preRegisterForm--icon mr2 ml3 color(border-blue-green)">
					<?php svgIcon('facebook-round-new', ' 0 0 32 32', 'facebook-round-new');?>
				</a>
				<a href="https://www.twitter.com/" target="_blank" class="c-preRegisterForm--icon color(border-blue-green)">
					<?php svgIcon('twitter-round-new', ' 0 0 32 32', 'twitter-round-new');?>
				</a>
			</p>
			<form class="c-preRegisterForm__form large-10 large-centered center">
				<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Name">
				<input class="c-preRegisterForm__form__input PelvivaCopy-s color(mid-grey) mb3 mb0-xl mr3-xl" type="text" placeholder="Enter Email Address">
				<input class="c-preRegisterForm__form__submit btn btn--contact bck-color(border-blue-green)" type="submit" name="submit" value="Pre-Register">
			</form>
		<?php endif; ?>	
		</div>

	</div>

</section>