<section class="l-container c-secondaryCarousel__wrapper">
      <div class="columns small-12 large-6 col-padding">
        <p class="PelvivaSubheader color(blue-green) bold mb4-s mb6-xl">Convenient &amp; discreet</p>

        <p class="PelvivaCopy-xl color(puerto-rico) bold mb3">Fits into your busy life</p>
        <ul class="list list--mark-grey list--mark-grey--l mb4-s mb5-l">
        	<li class="list-item PelvivaCopy-xl semibold color(blue-green) mb3">Can be used anywhere, even on the move</li>
        	<li class="list-item PelvivaCopy-xl semibold color(blue-green) mb3">Only 30 minutes per treatment</li>
        </ul>

        <p class="PelvivaCopy-xl color(puerto-rico) bold mb3">Easy to use</p>
        <ul class="list list--mark-grey list--mark-grey--l mb4-s mb6-l">
        	<li class="list-item PelvivaCopy-xl semibold color(blue-green) mb3">Comfortable</li>
        	<li class="list-item PelvivaCopy-xl semibold color(blue-green) mb3">Disposable</li>
        </ul>
      </div>

      <div class="c-secondaryCarousel col-no-padding columns small-12 large-6"></div>
</section>

<div class="c-secondaryCarousel__quotes l-container touch-fix clearfix">    
  <div class="columns small-12 large-6 col-padding">
    <p class="PelvivaCopy-xl color(puerto-rico)">“Pelviva was extremely easy to use with very clear instructions included…it was actually rather relaxing”</p>
    <p class="PelvivaCopy bold color(blue-green)">- Lisa-Marie, 30</p>
  </div>
  <div class="columns small-12 large-6 col-padding">
    <p class="PelvivaCopy-xl color(puerto-rico)">“[I] found it very discreet, easy to use and actually makes a difference to your Pelvic Floor control”</p>
    <p class="PelvivaCopy bold color(blue-green)">- Carolyn, 48</p>
  </div>
</div>