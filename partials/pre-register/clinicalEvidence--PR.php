<section class="l-container">

    <div class="c-clinicalEvidence c-clinicalEvidence--PR">

      <div class="column col-padding c-clinicalEvidence__heading">
        <p class="PelvivaSubheader color(blue-green) bold">Clinical Evidence</p>
      </div>

      <div class="c-clinicalEvidence__child-wrapper w-100">

      	<div class="column small-12 xlarge-6- c-clinicalEvidence__Child col-padding c-clinicalEvidence__Child--left">
          <p class="PelvivaCopy-xl color(blue-green) semibold mb2">Pelviva has been clinically proven to be safe and effective in a rigorous testing programme.</p>

          <p class="PelvivaCopy-xl color(blue-green) semibold mb4">After 12 weeks of regularly using Pelviva, women reported:</p>


          <div class="clearfix">
            <img class="c-clinicalEvidence__icon pl0 column" src="../../assets/img/site-wide/clinicallyProven4x.svg"/>
            <img class="c-clinicalEvidence__icon pl0 column" src="../../assets/img/site-wide/clinicallyProven84.svg"/>
          </div>
          <?php if(1===0): ?>
          <p class="PelvivaCopy color(mid-grey)">A four times greater reduction in the impact that bladder leaks had on their lives when compared to women doing their own Pelvic Floor exercises.</p>
          <?php endif; ?>
          <div class="clearfix"></div>
      	</div>

        <?php if(1===0): ?>
        <div class="column small-12 xlarge-6 c-clinicalEvidence__Child c-clinicalEvidence__Child--right col-padding c-clinicalEvidence__icons-container">
          <img class="c-clinicalEvidence__icon mb3 mb0-m mb4-xl" src="../../assets/img/site-wide/clinicallyProven4x.svg"/>
          <img class="c-clinicalEvidence__icon" src="../../assets/img/site-wide/clinicallyProven84.svg"/>
        </div>
        <?php endif; ?>

      </div>

    </div>
</section>


<section class="l-container touch-fix">
	<div class="columns small-12 col-padding small-centered">
		<p class="PelvivaCopy-xl color(puerto-rico) center">“I noticed a big reduction in ‘desperate’ situations and found myself using panty liners a lot less frequently”</p>
		<p class="PelvivaCopy bold color(blue-green) center">- Carolyn, 48</p>
	</div>
</section>
