<section class="l-container bck-color(light-green) touch-fix">

    <div class="c-clinicalEvidence">
      <div class="column col-padding c-clinicalEvidence__heading">
        <p class="PelvivaSubheader color(blue-green) bold">Clinical Evidence</p>
      </div>

        <div class="c-clinicalEvidence__child-wrapper w-100">

          <div class="column small-12 xlarge-6- c-clinicalEvidence__Child col-padding c-clinicalEvidence__Child--left">
            <p class="PelvivaCopy-xl color(blue-green) semibold">After 12 weeks of regularly </br> using Pelviva<sup>®</sup> women reported:</p>
            <div class="mb3 mb0-m mb4-xl mt3 mt0-m mt4-xl clearfix">
            <img class="c-clinicalEvidence__icon pl0 column" src="../../assets/img/site-wide/clinicallyProven4x.svg"/>
            <img class="c-clinicalEvidence__icon pl0 column" src="../../assets/img/site-wide/clinicallyProven84.svg"/>
          </div>
            <?php if(1===0): ?>
            <p class="PelvivaCopy color(mid-grey)">A four times greater reduction in the impact that bladder leaks had on their lives when compared to women doing their own Pelvic Floor exercises.</p>
            <?php endif; ?>
            <p class="PelvivaCopy-xl color(blue-green) color(green)- c-clinicalEvidence__borderText- semibold">Pelviva has been rigorously tested for safety and efficacy</p>
          </div>
          <?php if(1===0): ?>
          <div class="column small-12 xlarge-6 c-clinicalEvidence__Child c-clinicalEvidence__Child--right col-padding c-clinicalEvidence__icons-container">
            <img class="c-clinicalEvidence__icon mb3 mb0-m mb4-xl" src="../../assets/img/site-wide/clinicallyProven4x.svg"/>
            <img class="c-clinicalEvidence__icon" src="../../assets/img/site-wide/clinicallyProven84.svg"/>
          </div>
          <?php endif; ?>

        </div>

      </div>

    </div>


</section>

