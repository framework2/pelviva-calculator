<section class="l-container testimonialCards bck-color(light-green-blue)">

  <div class="row__inner">

    <!-- Testimonial 1 -->
    <div class="column small-12 xxlarge-6 mb3-s mb4-l">
      <div class="testimonialCard col-padding bck-color(white)">
        <p class="PelvivaCopy-l pl4-xxl pr4-xxl mb3-xxl color(dark-grey)">
          “After I run 12k my leakage can be uncontrollable. This morning I tensed my pelvic floor and was able to control it. Fantastic Pelviva has made a real difference!”
        </p>

        <div class="testimonialCard__details">
          <p class="PelvivaCopy-l mb0 color(dark-grey)">Rachel: Runner and tri-athlete</p>
          <p class="PelvivaCopy-l color(dark-grey)">Age: 43</p>
        </div>
      </div>
    </div>

    <!-- Testimonial 2 -->
    <div class="column small-12 xxlarge-6 mb3-s mb4-l">
      <div class="testimonialCard col-padding bck-color(white)">
        <p class="PelvivaCopy-l pl4-xxl pr4-xxl mb3-xxl color(dark-grey)">
          “After I run 12k my leakage can be uncontrollable. This morning I tensed my pelvic floor and was able to control it. Fantastic Pelviva has made a real difference!”
        </p>

        <div class="testimonialCard__details">
          <p class="PelvivaCopy-l mb0 color(dark-grey)">Rachel: Runner and tri-athlete</p>
          <p class="PelvivaCopy-l color(dark-grey)">Age: 43</p>
        </div>
      </div>
    </div>

    <!-- Testimonial 3 -->
    <div class="column small-12 xxlarge-6 mb3-s mb4-l mb0-xl">
      <div class="testimonialCard col-padding bck-color(white)">
        <p class="PelvivaCopy-l pl4-xxl pr4-xxl mb3-xxl color(dark-grey)">
          “After I run 12k my leakage can be uncontrollable. This morning I tensed my pelvic floor and was able to control it. Fantastic Pelviva has made a real difference!”
        </p>

        <div class="testimonialCard__details">
          <p class="PelvivaCopy-l mb0 color(dark-grey)">Rachel: Runner and tri-athlete</p>
          <p class="PelvivaCopy-l color(dark-grey)">Age: 43</p>
        </div>
      </div>
    </div>

    <!-- Testimonial 4 -->
    <div class="column small-12 xxlarge-6">
      <div class="testimonialCard col-padding bck-color(white)">
        <p class="PelvivaCopy-l pl4-xxl pr4-xxl mb3-xxl color(dark-grey)">
          “After I run 12k my leakage can be uncontrollable. This morning I tensed my pelvic floor and was able to control it. Fantastic Pelviva has made a real difference!”
        </p>

        <div class="testimonialCard__details">
          <p class="PelvivaCopy-l mb0 color(dark-grey)">Rachel: Runner and tri-athlete</p>
          <p class="PelvivaCopy-l color(dark-grey)">Age: 43</p>
        </div>
      </div>
    </div>

  	<div class="clearfix"></div>
  </div>
</section>
