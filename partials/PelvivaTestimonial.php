 <section class="l-container c-PelvivaTestimonial touch-fix">

  	<div class="column small-12 xxlarge-7 col-no-padding c-PelvivaTestimonial__Child c-PelvivaTestimonial__image c-PelvivaTestimonial__Child--left">

      <a href="#" class="c-PelvivaTestimonial__caption">
        <p class="PelvivaCopy-l color(dark-grey) bold center">Read about women like you who have tried Pelviva<sup>®</sup></p>
        <div class="c-PelvivaTestimonial__caption__icon color(dark-grey)"><?php svgIcon('icon-circleArrow', '0 0 34.96 34.96', 'icon-circleArrow');?></div>
      </a>

    </div>

 	 <div class="column small-12 xxlarge-5 col-padding c-PelvivaTestimonial__Child c-PelvivaTestimonial__Child--right bck-color(soft-moonstone)">


  <p class="PelvivaCopy-l color(mid-grey)">“After exercise my leakage can be uncontrollable.</p>

  <p class="PelvivaCopy-l color(mid-grey)">“This morning I tensed my Pelvic Floor and was able to control it. Fantastic!</p>

  <p class="PelvivaCopy-l color(mid-grey) bold">Pelviva<sup>®</sup> has made a</br> real difference!”</p>

  <p class="PelvivaCopy-l color(green)">Rachel – Aerobics Instructor</br>Age 33</p>

	  </div>


</section>
