<section class="l-container c-TypesOfBladderLeakage bck-color(border-light-green)">

	<div class="column small-12 xlarge-5 col-no-padding c-TypesOfBladderLeakage__main-image"></div>

	<div class="column small-12 xlarge-7 c-TypesOfBladderLeakage__text-wrapper col-padding">
		<h2 class="PelvivaSubheader color(green) mb3 bold">Types of Bladder Leakage</h2>

		<p class="PelvivaCopy color(blue-green) bold c-WhatIsBladderLeakage__text">There are several types of bladder leakage, including:</p>

		<p class="PelvivaCopy color(dark-grey)  c-TypesOfBladderLeakage__text mb3"><span class="uppercase semibold color(green)">Stress leakage</span> – when urine leaks, at times when your bladder is under pressure; for example as a result of activity such as exercise, or when you cough, laugh or sneeze. Often the first time women realise they have a bladder leakage problem is when they experience ‘stress leakage’ when they start running or jumping with the kids on the trampoline.</p>

		<p class="PelvivaCopy color(dark-grey)  c-TypesOfBladderLeakage__text mb3"><span class="uppercase semibold color(green)">Urge or urgency leakage</span> – when urine leaks as you feel a sudden intense urge to pass urine and not being able hold on and make it to the toilet in time.</p>

		<p class="PelvivaCopy color(dark-grey)  c-TypesOfBladderLeakage__text mb4"><span class="uppercase semibold color(green)">Mixed Bladder Leakage</span> – when both types are present. </p>

		<p class="PelvivaCopy-l color(blue-green)  c-TypesOfBladderLeakage__text c-TypesOfBladderLeakage__text--bottom"><span class="bold">pelviva<sup>®</sup></span> is the only treatment to provide ONE combined treatment for both stress and urgency bladder leakage.</p>


	</div>

	<div class="clearfix"></div>

</section>
