<div class="secondaryCarousel l-container">

<section class="l-container col-no-padding c-secondaryCarousel__wrapper carousel-cell">
      <div class="columns small-12 large-6 col-padding">
        <p class="PelvivaSubheader color(blue-green) bold mb4-super">Convenient &amp; easy to use</p>
        <p class="PelvivaCopy-xl color(blue-green) semibold mb4-super">Easy to use at any time of the day. Pelviva fits into women’s busy lives.</p>
        <a class="btn btn--main" href="#">Learn More</a>
      </div>

      <div class="c-secondaryCarousel columns small-12 large-6">
      </div>
</section>

<section class="l-container col-no-padding c-secondaryCarousel__wrapper carousel-cell">
      <div class="columns small-12 large-6 col-padding">
        <p class="PelvivaSubheader color(blue-green) bold mb4-super">Simple &amp; discreet</p>
        <p class="PelvivaCopy-xl color(blue-green) semibold mb4-super">The Pelviva reactive pulse mimics the way your body works naturally causing your Pelvic Floor muscles to contract as they should, helping you recognise the feeling of doing Pelvic Floor muscle exercises correctly. </p>
        <a class="btn btn--main" href="#">Learn More</a>
      </div>

      <div class="c-secondaryCarousel c-secondaryCarousel2 columns small-12 large-6">
      </div>
</section>

<section class="l-container col-no-padding c-secondaryCarousel__wrapper carousel-cell">
      <div class="columns small-12 large-6 col-padding">
        <p class="PelvivaSubheader color(blue-green) bold mb4-super">Comfortable fit</p>
        <p class="PelvivaCopy-xl color(blue-green) semibold mb4-super">Body Responsive Foam adapts to your individual size and shape for a comfortable fit.</p>
        <a class="btn btn--main" href="#">Learn More</a>
      </div>

      <div class="c-secondaryCarousel c-secondaryCarousel3 columns small-12 large-6">
      </div>
</section>

</div>
