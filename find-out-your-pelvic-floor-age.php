<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Find out your Pelvic Floor age';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

    <!-- Fourth Page -->
    <?php include 'partials/calculatorHero.php' ?>
    <?php include 'partials/PelvicFloorAgeCalculator.php' ?>

    <?php include_once 'partials/footer.php' ?>
    <script src="assets/js/vue/vue-app.js"></script>
  </body>
</html>
