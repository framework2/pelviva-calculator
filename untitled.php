<section class="l-container c-WhyPelviva--PR bck-color(light-green)">




  	<div class="column small-12 large-6 col-padding  c-WhyPelviva--PR__Child--left bck-color(green)">

      <h1 class="PelvivaSubheader c-Whyelviva__Header color(white) semibold">Why <span class="bold">peliviva® ?</span></h1>

      <p class="PelvivaCopy color(blue-green)">
        Pelviva is a singlue use, disposable medical device made from soft compressible foam. Each Pelviva contains a microprocessor that delivers the revolutionary <span class="PelvivaCopySpan">Pelviva reactive pulse technology</span> directly to your pelvic floor muslces, exercising both types of muscle fibre within your pelvic floor. The Pelviva reactive pulse mimics the way your body works naturally, stimulating your pelvic floor muscles to perform perfect pelvic floor contractions every time. Helping you recognise the feeling of correct pelvic floor muscle exercises.
      </p>

      <div class="c-WhyPelviva--PR__ImageWrapper">
      <img class="c-WhyPelviva--PR__Image" src="/assets/img/what-is-pelviva/ReactivePulse.png" alt=""></img>
      </div>

  	</div>

    <div class="column small-12 large-6 col-padding  c-WhyPelviva--PR__Child--left bck-color(green)">

      <p class="PelvivaCopy b color(blue-green) c-WhyPelviva--PR__Copy--top">The two types of muscle ﬁbre within your Pelvic Floor:</p>

      <div class="c-WhyPelviva--PR__Fibre1 mb3-s">
        <p class="PelvivaCopy-l color(blue-green) mb0">POWER FIBRES</p>
        <p class="PelvivaCopy color(white)">Need to have speed and strength so they can stop leakage when you cough, sneeze, laugh or exercise.</p>
      </div>

      <div class="c-WhyPelviva--PR__Fibre2 mb2-s mb3-l">
        <p class="PelvivaCopy-l color(blue-green) mb0">ENDURANCE FIBRES</p>
        <p class="PelvivaCopy color(white)">Need to work to help you hold on when your urgently need the toilet and to stop you needing the toilet too frequently. Because of the two fiber types, different exercises are rquired to imrpove their individual performance.</p>
      </div>

      <p class="PelvivaCopy color(blue-green) c-WhyPelviva--PR__Copy--bottom"><span class="bold">Pelviva®</span> is the only treatment to provide one combined treatment for both stress and urinary incontinence.</p>

    </div>






</section>
