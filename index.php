<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Pelviva</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" href="https://use.typekit.net/kxq8qoe.css">
	<style type="text/css">
		html,body {
			font-family: 'proxima-soft', sans-serif;
			font-size: 16px;
			padding: 0;
			margin: 0;
		}


		a,
		a:hover,
		a:active {
			text-decoration: none;
		}

		.c-navigation {
			list-style-type: none;
		}

		.c-navigation__item {
			margin-bottom: 20px;
		}

		.c-navigation__link {
			color: #296e89;
			font-size: 18px;
			text-decoration: none;
		}

		.c-navigation__link:hover {
			color: #ef8181;
		}

		.c-subnav__item {
			margin-top: 10px;
		}

		.c-subnav__link {
			color: #194b63;
		}

		.c-subnav__link:hover {
			color: #ef8181;
		}
	</style>
</head>
<body>

	<ul class="c-navigation">
		<li class="c-navigation__item">
			<a href="pre-register.php" class="c-navigation__link"">Pre Register</a>
			<br>
			<a href="home.php" class="c-navigation__link"">Home</a>
			<br>
			<a href="what-is-pelviva.php" class="c-navigation__link">What is Pelviva?</a>
			<br>
			<a href="1-in-every-3-women.php" class="c-navigation__link">1 in every 3 women</a>
			<br>
			<a href="find-out-your-pelvic-floor-age.php" class="c-navigation__link">Find out your Pelvic Floor age</a>
			<br>
			<a href="6-reasons-to-use-pelviva.php" class="c-navigation__link">6 Reasons to use Pelviva</a>
			<br>
			<a href="testimonials.php" class="c-navigation__link">Testimonials</a>
			<br>
			<a href="faqs.php" class="c-navigation__link">FAQs</a>
			<br>
			<a href="contact.php" class="c-navigation__link">Contact</a>
			<br>
			<a href="example.php" class="c-navigation__link">Example</a>
			<br>
			<a href="register.php" class="c-navigation__link">Register</a>
		</li

	</ul>

</body>
</html>
