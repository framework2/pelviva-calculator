<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'example';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/menu.php' ?>
    <?php include 'partials/header.php' ?>

    <div class="l-container c-contactFormWrapper">

      <div class="xlarge-6 xlarge-centered column mb4">
        
        <h1 class="PelvivaHeader color(blue-green) mb3 bold">Example Page Heading</h1>
        <h2 class="PelvivaSubheader color(blue-green) mb4 semibold">This is a sub heading</h2>

        <p class="PelvivaCopy color(mid-grey) mb3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

        <p class="PelvivaCopy color(mid-grey) mb3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

        <p class="PelvivaCopy color(mid-grey) mb3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>

        <p class="PelvivaCopy color(mid-grey) mb3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
      </div>


    </div>


    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
