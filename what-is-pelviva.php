<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'What is Pelviva';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

        <!-- Second Page -->
        <?php include 'partials/WhatIsPelviva.php' ?>
        <?php include 'partials/PelvivaTechnology.php' ?>
        <?php include 'partials/DynamicCRC.php' ?>
        <?php include 'partials/WhyPelviva.php' ?>
        <?php include 'partials/clinicalEvidence.php' ?>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
