class BladderLeakage {

	/**
	  * constructor
	 */
	constructor() {

		let ShowBtn = document.querySelector('.showInfoBox');
		let InfoBox = document.querySelector('.c-WhatIsBladderLeakage__answer');

		this.clickShow(ShowBtn, InfoBox);
	}

	clickShow(btn, box) {

		if(_.isNull(btn,box) == false) {

			btn.addEventListener('click', (e) => {
				btn.classList.add('btn--main--active')
				box.classList.remove('c-WhatIsBladderLeakage__answer--hidden');
		 		box.classList.add('c-WhatIsBladderLeakage__answer--visible');
			}, true);

		};

	};
}

// Export Module
module.exports = BladderLeakage;

