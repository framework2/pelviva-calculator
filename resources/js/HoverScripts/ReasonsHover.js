class ReasonsHover {

	/**
	  * constructor
	 */
	constructor() {

		let hoverIcons = document.querySelectorAll('.c-reasons__icon-wrapper');

		this.textboxes = document.querySelectorAll('.c-reasons__textbox');
		this.corresponsiveHover(hoverIcons);
	}

	corresponsiveHover(icons) {

		Array.prototype.forEach.call(icons, (el, i) => {

			this.toggleFirstElement(el, i);

			el.addEventListener('mouseover', (e) => {
				let item = el.getAttribute('id');

				if(_.isNull(item) == false) {
					
					let element = document.querySelector('#textbox__' + item);
					if (_.isNull(element) == false) {
						this.hideElements();
						element.classList.add('show');
					}
				}
			}, true);

			el.addEventListener('click', (e) => {
				let item = el.getAttribute('id');

				if(_.isNull(item) == false) {
					let element = document.querySelector('#textbox__' + item);
					if (_.isNull(element) == false) {
						this.hideElements();
						element.classList.add('show');
					}
				}
			}, true);
		});
	}

	toggleFirstElement(el, index) {
		if (index == 0) {
			let item = el.getAttribute('id');

			if(_.isNull(item) == false) {
				let element = document.querySelector('#textbox__' + item);
				if (_.isNull(element) == false) {
					this.hideElements();
					element.classList.add('show');
				}
			}
		}
	}

	hideElements() {
		Array.prototype.forEach.call(this.textboxes, (el, i) => {
			el.classList.remove('show');
		});
	}
	
}

// Export Module
module.exports = ReasonsHover;

