class DynamicCRCHover {

	/**
	  * constructor
	 */
	constructor() {

		let hoverIcons = document.querySelectorAll('.c-DynamicCRC__icon-wrapper');
		this.textWrapper = document.querySelector('.c-DynamicCRC__text-wrapper');
		this.textboxes = document.querySelectorAll('.c-DynamicCRC__textbox');

		if ( _.isNull(hoverIcons) == false && _.isNull(this.textboxes) == false ) {
			this.corresponsiveHover(hoverIcons);
		}

		if ( _.isNull(this.textWrapper) == false && _.isNull(this.textboxes) == false ) {
			this.resize();
		}
	}

	corresponsiveHover(icons) {

		Array.prototype.forEach.call(icons, (el, i) => {

			this.toggleFirstElement(el, i);

			el.addEventListener('mouseover', (e) => {
				let item = el.getAttribute('id');

				if(_.isNull(item) == false) {
					
					let element = document.querySelector('#textbox__' + item);
					if (_.isNull(element) == false) {
						this.hideElements();
						element.classList.add('hoverShow');
					}
				}
			}, true);

			el.addEventListener('click', (e) => {
				let item = el.getAttribute('id');

				if(_.isNull(item) == false) {
					let element = document.querySelector('#textbox__' + item);
					if (_.isNull(element) == false) {
						this.hideElements();
						element.classList.add('hoverShow');
					}
				}
			}, true);
		});
	}

	resize() {
		let heights = [];

		Array.prototype.forEach.call(this.textboxes, (box, i) => {
				let boxHeight = parseFloat(window.getComputedStyle(box, null).getPropertyValue("height").slice(0,-2));
				heights.push(boxHeight);
		});

		let biggest = Math.max.apply(null, heights);

		this.textWrapper.style.setProperty( 'height', biggest.toString() + 'px');
	}

	toggleFirstElement(el, index) {
		if (index == 0) {
			let item = el.getAttribute('id');

			if(_.isNull(item) == false) {
				let element = document.querySelector('#textbox__' + item);
				if (_.isNull(element) == false) {
					this.hideElements();
					element.classList.add('hoverShow');
				}
			}
		}
	}

	hideElements() {
		Array.prototype.forEach.call(this.textboxes, (el, i) => {
			el.classList.remove('hoverShow');
		});
	}
	
}

// Export Module
module.exports = DynamicCRCHover;

