var Flickity = require('flickity');

class Slider{
  constructor(){
    this.carousel = document.querySelector(".mainCarousel");
    this.secondaryCarousel = document.querySelector(".secondaryCarousel");
    if (_.isNull(this.carousel) == false) {
        this.initCarousel(this.carousel);
    }
    if (_.isNull(this.secondaryCarousel) == false) {
        this.initCarousel(this.secondaryCarousel);
    }

  }
  initCarousel(carousel){
    var flkty = new Flickity( carousel, {
      // options
      cellAlign: 'center',
      contain: true,
      prevNextButtons: false,
      wrapAround: true,
      imagesLoaded: true,
      adaptiveHeight: true,
      // autoPlay: 4500,
      prevNextButtons: false
      });
  }
}
module.exports = Slider;
