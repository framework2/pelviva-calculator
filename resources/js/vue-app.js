// Vue Import
import Vue from 'vue'
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate, { inject: false });


// Debug Options
Vue.config.debug = true; 
Vue.config.devtools = true;

// Component Imports
import Questions from './components/Questions.vue';
import EnterEmail from './components/EnterEmail.vue';
import Question from './components/Question.vue';
import Result from './components/Result.vue';

// Initialise Components
Vue.component('enter-email',EnterEmail);
Vue.component('question',Question);
Vue.component('result',Result);


// Root Vue Instance
var app = new Vue({

	el: '#calculator',

	data() {
		return {
			
		}
	},
	render : h => h(Questions),
	
	methods: {}

});
