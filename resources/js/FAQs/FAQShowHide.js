class FAQShowHide {

	/**
	  * constructor
	 */
	constructor() {
		let questionBlocks = document.querySelectorAll('.c-faqs__question-block');

		if(_.isNull(questionBlocks) == false) {
			this.showHide(questionBlocks);
		}
	}

	showHide(questionBlocks) {

		Array.prototype.forEach.call(questionBlocks, (el, i) => {

			let question = el.querySelector('.c-faqs__question');

			question.addEventListener('click', (e) => {

				if ( el.classList.contains('c-faqs__question-block--active') ) {
					el.classList.remove('c-faqs__question-block--active');
				} else {
					el.classList.add('c-faqs__question-block--active');
				};

			}, true);

		});
	
	}

};

// Export Module
module.exports = FAQShowHide;
