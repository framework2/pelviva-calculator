class SmoothScroll {

	/**
	  * constructor
	 */
	constructor() {
	
		// Query all elements that have the SmoothScroll class
		let toggle = document.querySelectorAll('.SmoothScroll');

		// Check if any above elements exist
		if (_.isNull(toggle) == false) {

			// If so run the bind events function using the Queried elements
			this.bindEvents(toggle);
		}
	}

	bindEvents(elements) {


		// Loop through the queried elements
		Array.prototype.forEach.call(elements, (el, i) => {
			el.addEventListener('click', (e) => {
				e.preventDefault();

				// Store the elements / properties needed
				let currentPosition = document.documentElement.scrollTop;
				let elements = el.getAttribute('href');
				let block = document.querySelector(elements);

				// Check is the properties are there
				if (_.isNull(block) == false) {

					// If so use these properties in the ScrollScript function
					this.ScrollScript(document.scrollingElement || document.documentElement, "scrollTop", "", currentPosition, block.offsetTop, 500, true);
				}
			});
		});
	}

	// Scroll Script function
	ScrollScript(elem, style, unit, from, to, time, prop) {

		 if (!elem) {
		     return;
		 }

		 var start = new Date().getTime(),
		     timer = setInterval(function () {
		         var step = Math.min(1, (new Date().getTime() - start) / time);
		         if (prop) {
		             elem[style] = (from + step * (to - from))+unit;
		         } else {
		             elem.style[style] = (from + step * (to - from))+unit;
		         }
		         if (step === 1) {
		             clearInterval(timer);
		         }
		     }, 25);
		 if (prop) {
		     elem[style] = from+unit;
		 } else {
		     elem.style[style] = from+unit;
		 }
	}
	
}

// Export Module
module.exports = SmoothScroll;

