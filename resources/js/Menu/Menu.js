class Menu {

	/**
	  * constructor
	 */
	constructor() {

		let MenuToggle = document.querySelectorAll('.menu-toggle');
		let Menu = document.getElementById('menu');
		let ClassName = 'open';

		this.Menu(MenuToggle, Menu, ClassName);

	}

	Menu(toggle, menu , className) {
		Array.prototype.forEach.call(toggle, (el, i) => {
			el.addEventListener('click', (e) => {
				if ( menu.classList.contains(className) ) {
			  		menu.classList.remove(className);
				} else {
			  		menu.classList.add(className);
				};

			});
		});
	}
	
}

// Export Module
module.exports = Menu;

