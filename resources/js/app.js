/**
 * Dependencies
 */


 // Load the full build.
 var _ = require('lodash');

/**
 * variables
 */
const SmoothScroll = require('./Scroll/SmoothScroll');
const Menu = require('./Menu/Menu');
const Slider = require('./Slider/Slider');
const PelvivaTechnologyHover = require('./HoverScripts/PelvivaTechnologyHover');
const DynamicCRCHover = require('./HoverScripts/DynamicCRCHover');
const BladderLeakage = require('./ClickEvents/BladderLeakage');
const ClickToPlayPause = require('./Video/ClickToPlayPause');
const FAQShowHide = require('./FAQs/FAQShowHide');
const ReasonsHover = require('./HoverScripts/ReasonsHover');


 const Breakpoint     = require('./Responsive/Breakpoint');
 window.Breakpoint    = Breakpoint;
 Breakpoint.init();
 window.addEventListener("resize",() => {
   Breakpoint.refreshValue();
 }, false);
 /*
 * add in our responsive grid
 * for testing purposes // OnKeyDown
 */
document.onkeydown = require('./Helpers/OnKeyDown');
/*
 * Our Object to run all this
 * which is a class to help
 * us work with resizes
 */
class FwJs {
    constructor(){
      this.initLoad = false;
      this.reloaders = [];
    }
    init(){
      this.initLoad = true;
      this.reloaders.push(new DynamicCRCHover());
      new SmoothScroll();
      new Menu();
      new Slider();
      new PelvivaTechnologyHover();
      new BladderLeakage();
      new ClickToPlayPause();
      new FAQShowHide();
      new ReasonsHover();
    }
   /*
    * the resizer runs all classes that assign
    * themselves the resize method
    */
    resize(){
      this.reloaders.forEach((element,index) => {
        if(typeof element.resize === 'function'){
          element.resize();
        }
      });
    }
};
// CREATE the main object
let FwJsObj = new FwJs();
/*
 * one resize event listener needed which runs through all Our
 * classes that want to run resize events
 */
window.addEventListener("resize", _.debounce(() => FwJsObj.resize(), 300), false);
/*
 * lets get it ready (as in only use when dom content is ready)
 *
 * @param fn the function being passed in to be used
 */
function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
ready(FwJsObj.init());
