class ClickToPlayPause {

	/**
	  * constructor
	 */
	constructor() {

		let videos = document.querySelectorAll('video');
		this.videoIsPlaying = false;
		
		this.clickToPlayPause(videos);
	}

	clickToPlayPause(videos) {

		if(_.isNull(videos) == false) {

			Array.prototype.forEach.call(videos, (el, i) => {

				el.addEventListener('click', (e) => {

					if (this.videoIsPlaying == true) {
						el.pause();
						this.videoIsPlaying = false;
					} else {
						el.play();
						this.videoIsPlaying = true;
						el.controls = true;
					};

				});

			});

		};

	}

}

// Export Module
module.exports = ClickToPlayPause;
