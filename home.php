<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Home';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

        <!-- First Page -->
        <?php include 'partials/homePageSlider.php' ?>
        <?php include 'partials/howPelvivaWorks.php' ?>
        <?php include 'partials/secondaryCarousel.php' ?>
        <?php include 'partials/DiscoverYourPelvicFloorAge.php' ?>
        <?php include 'partials/PelvivaTestimonial.php' ?>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
