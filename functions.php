<?php
function ourFunction($boolean = false){

}

function runSub($key, $value){

}

function get_footer($name = null) {
    if ( is_null($name)) {
        include("partials/footer.php");
    } else {
        include("partials/".$name.'.php');
    }
}


/**
  * svgIcon
  *
  * @param String $icon
  *
  * @return NULL
  */
function svgIcon($icon = null, $viewBox = '0 0 100 100', $className = ''){
  if(is_null($icon)){
    return '';
  }
  echo '<svg viewBox="'.$viewBox.'" class="c-icon '.$className.'"><use xlink:href="#' . $icon . '"></use></svg>';
}



/**
  * pieChart
  *
  * @param Integer $percentage
  *
  * @return NULL
  */
function pieChart($percentage = null, $color = '#000000'){
  if(is_null($percentage)){
    return '';
  }
  echo '<svg data-percentage="'.$percentage.'" data-color="'.$color.'" class="pie-chart-svg" viewBox="-1 -1 2 2" style="transform: rotate(-90deg)">
  <style>
  .cls-9{font-size:53.21px;}
  .cls-11,
  .cls-9{fill:#fff;font-family:MyriadPro-Regular, Myriad Pro;}
  .cls-10{letter-spacing:-0.01em;}.cls-11{font-size:31.02px;}
  </style>
          <linearGradient id="linear-gradient" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" stop-color="#eaeaeb"/>
            <stop offset="1%" stop-color="#eaeaeb"/>
            <stop offset="31%" stop-color="#e3e3e4"/>
            <stop offset="75%" stop-color="#cfd0d2"/>
            <stop offset="100%" stop-color="#c1c2c4"/>
          </linearGradient>
          <circle cx="0" cy="0" r="0.75" stroke="transparent" fill="url(#linear-gradient)"></circle>
        </svg>
  ';
}
