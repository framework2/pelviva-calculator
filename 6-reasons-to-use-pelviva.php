<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = '6 Reasons to use Pelviva';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

    <!-- Fifth Page -->
    <?php include 'partials/reasonsHero.php' ?>
    <?php include 'partials/reasons.php' ?>
    <?php include 'partials/InstructionalVideo.php' ?>
    <?php include 'partials/HowOften.php' ?>
    <?php include 'partials/HowDoYouKnow.php' ?>
    <?php include 'partials/MaintainYourPelvicFloorMuscles.php' ?>
    <?php include 'partials/WhoShouldntUsePelviva.php' ?>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
