var elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');

elixir.config.publicPath = 'assets/';
elixir.config.assetsPath = 'resources/';
elixir.config.versioning.buildFolder = 'assets/';

elixir(function (mix) {

    mix.sass('style.scss', elixir.config.assetsPath + 'css');
    mix.webpack('app.js');
    mix.webpack('vue-app.js', elixir.config.publicPath + 'js/vue');
    // mix.sass('app.scss', elixir.config.assetsPath);
    //
    //
    // ///// function(src, output, baseDir, options) ////
     // mix.browserify('main.js');
    // mix.webpack('app.js');
    //
    // // function(styles, output, baseDir) //
    mix.styles([
        'style.css'
    ]);

    // mix.browserSync({
    //    proxy : 'oma-forms.dev'
    // });

});
