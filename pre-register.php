<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Pre Register';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/pre-register/pre-register-header.php' ?>

        <?php include 'partials/pre-register/introducing-pelviva.php' ?>
        <?php include 'partials/pre-register/one-in-three-women.php' ?>
        <?php include 'partials/pre-register/whatIsPelviva.php' ?>
        <?php include 'partials/pre-register/pre-register-form--social.php' ?> 
        <?php include 'partials/pre-register/convenient.php' ?>
        <?php include 'partials/pre-register/whyPelvivaPR.php' ?>
        
        <div class="l-container">
          <?php include 'partials/pre-register/pre-register-form-2.php' ?>
        </div>

        <?php include 'partials/pre-register/DynamicCRC--PR.php' ?>
        <?php include 'partials/pre-register/clinicalEvidence--PR.php' ?> 
        <?php include 'partials/pre-register/pre-register-form.php' ?> 
  
    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
