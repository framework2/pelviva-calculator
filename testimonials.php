<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = 'Testimonials';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

    <!-- Sixth Page -->
    <?php include 'partials/PelvivaTestimonialAlternate.php' ?>
    <?php include 'partials/testimonialCards.php' ?>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
