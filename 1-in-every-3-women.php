<?php
include 'partials/htaccess-login.php';
include 'functions.php';
$title = '1 in every 3 women';
?>
<!DOCTYPE html>
<html>
  <?php include_once 'partials/svg-sprite.php';
?>
  <?php include 'partials/head.php' ?>
  <body>
    <?php include 'partials/Menu.php' ?>
    <?php include 'partials/header.php' ?>

    <!-- Third Page -->
    <?php include 'partials/womenHero.php' ?>
    <?php include 'partials/WhatIsBladderLeakage.php' ?>
    <?php include 'partials/WhatIsThePelvicFloor.php' ?>
    <?php include 'partials/TypesOfBladderLeakage.php' ?>
    <?php include 'partials/YourNotAlone.php' ?>
    <?php include 'partials/WhyExerciseYourPelvicFloor.php' ?>

    <?php include_once 'partials/footer.php' ?>
  </body>
</html>
